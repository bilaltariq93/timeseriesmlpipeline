import pandas as pd
from datetime import datetime, timedelta
import os
import datetime as dt
import numpy as np
import time
from collections import OrderedDict
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report,confusion_matrix
from imblearn.over_sampling import RandomOverSampler
from imblearn.under_sampling import RandomUnderSampler
from mil.preprocessing import StandarizerBagsList
from mil.bag_representation import MILESMapping
from imblearn.over_sampling import SMOTE
from mil.models import RandomForestClassifier, SVC
from mil.models import SVC
from mil.trainer import Trainer
from mil.metrics import AUC
from mil.validators import KFold
from mil.metrics import Specificity
from mil.metrics import TrueNegatives
from mil.metrics import TruePositives
from mil.metrics import FalseNegatives
from mil.metrics import FalsePositives
from mil.metrics import Precision
from mil.metrics import Recall
from mil.metrics import AUC
from sklearn.metrics import roc_curve,roc_auc_score
from sklearn.metrics import auc
from mil.models import APR
pd.options.display.float_format = '{:20.20}'.format
from sklearn import preprocessing
import sys
import random
from sklearn.ensemble import AdaBoostClassifier
from sklearn import metrics
from sklearn.model_selection import cross_val_score
import logging
import argparse
from scipy.signal import butter
from scipy.signal import filtfilt
from scipy.signal import medfilt
import pandas as pd
import numpy as np
import os 
from collections import OrderedDict
import time
from scipy.stats import kurtosis, skew
from scipy.stats import iqr
import datetime
import copy as cp 
import math
from scipy.signal import welch
import argparse
import traceback

class BandPassFilter():
    
    def __init__(self):
        pass
    
    def CreatePathIfNotExists(self, path):
        if not os.path.exists(path):
            os.makedirs(path)
            return path
        else:
            return path

    def _BandPassFilter(self, X):
        fs = 50 # Sample Rate
        lowcut = 0.1 # Low Frequency
        highcut = 20 # High Frequency
        nyq = 0.5 * fs 
        low = lowcut / nyq
        high = highcut / nyq
        order = 6 #https://dsp.stackexchange.com/questions/37646/filter-order-rule-of-thumb/37662
        b, a = butter(order, [low, high], 'bandpass', analog = False)

        df = pd.DataFrame()
        columns = list(X.columns)
        columnsToRead = ['x1','x2','x3']

        for everyCol in range(0,X.shape[1]):
            if columns[everyCol] in columnsToRead:
                arr = X[columns[everyCol]].values
                arr = filtfilt(b,a,arr, axis = 0)
                df[columns[everyCol]] = arr
            else: 
                df[columns[everyCol]] = X[columns[everyCol]]

        return df

    def ReadFiles(self, pid, pathToReadFiles, window, overlap):
        p_id = pid
        p = {}

        pathToFiles = pathToReadFiles
        directories = os.listdir(pathToFiles)
        fdirectories = []
        for d in directories:
            if d == 'main.py' or d == 'run.sh' or d == '.ipynb_checkpoints':
                pass
            else:
                fdirectories.append(d)
        #print(fdirectories)
        for everyDay in fdirectories:
            pwd = os.listdir(pathToFiles +everyDay)
            for everyPWD in pwd:
                if(everyPWD == p_id):
                    path = pathToFiles + everyDay + '/' + everyPWD
                    allCSVs = os.listdir(path)
                    CSVName = str(window) + '_' + str(overlap) + '_' + everyPWD + '_' + everyDay + '.csv'
                    for everyCSV in allCSVs:
                        if(everyCSV == CSVName):
                            path = path+ '/' + everyCSV
                            try:
                                file = pd.read_csv(path)
                                if len(file) > 0:
                                    logging.info('File read at: %s', path)
                                    p[everyPWD + '_' + everyDay] = path
                                else:
                                    logging.info('File Read but is empty.' + str('CSVName'))
                                    raise Exception('File Read but is empty.' + str('CSVName'))
                            except:
                                logging.info('This file was not read: ' + path + '\nFile Size: ' + str(os.stat(path).st_size))

        p = OrderedDict(sorted(p.items()))
        p_0 = list(p.values())
        p_fileName = list(p.keys())

        return p_0, p_fileName

    def Worker(self, p_0, pathBf, window, overlap, fileName, p_id):
        for index in range(0,len(p_0)):
            logging.info('Current File: %s', str(fileName[index]))
            _bf = pd.read_csv(p_0[index])
            _bf = self._BandPassFilter(_bf)
            self.CreatePathIfNotExists(pathBf[index])
            logging.info('Path created for file: %s', str(pathBf[index]))
            _bf.to_csv(pathBf[index]+ (str(window) + "_" + str(overlap) + "_" +  fileName[index] + '.csv'), index=False)
            logging.info('File saved as: %s', str(pathBf[index]+ (str(window) + "_" + str(overlap) + "_" + fileName[index])+'.csv'))


    def Prepare(self, source, destination, pwd, window, overlap):
        logging.info('Check for Multiple pids...')
        pwd = pwd.split(';')
        logging.info('Total pwds: %s', len(pwd))

        for p_id in pwd:
            logging.info('Bandpass filter starting for' + p_id)
            logging.info('File Reading starting')

            p_0, fileName = self.ReadFiles(p_id, source, window, overlap)
            fileDate = [i.split('_', 1)[-1] for i in fileName]

            rootPathToBf = self.CreatePathIfNotExists(destination + '/020_BandPassFilter')
            pathBf = list()

            for fd in range(0, len(fileDate)):
                pathBf.append(rootPathToBf + '/' + fileDate[fd] + '/' + p_id + '/')

            self.Worker(p_0, pathBf, window, overlap, fileName, p_id)