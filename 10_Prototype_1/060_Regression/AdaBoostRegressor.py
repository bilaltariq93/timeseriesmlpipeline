import pandas as pd
from datetime import datetime, timedelta
import os
import datetime as dt
import numpy as np
import time
from collections import OrderedDict
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report,confusion_matrix
from sklearn.metrics import roc_curve,roc_auc_score
from sklearn.metrics import auc
pd.options.display.float_format = '{:20.20}'.format
from sklearn import preprocessing
import sys
sys.path.append(os.getcwd())
import random
from sklearn import metrics
from sklearn.model_selection import cross_val_score
import logging
import argparse
from sklearn.datasets import make_regression
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import RepeatedKFold
from sklearn.ensemble import AdaBoostRegressor
from collections import Counter
import matplotlib.pyplot as plt
from sklearn.tree import DecisionTreeRegressor

class BoI():
    
    def __init__(self):
        pass
    
    def BagLabel(self, _df, _X, fileNames, _window):
        bagCounter = 1
        allDfs = list()
        for index in range(0, len(_df)):
            df = _df[index]
            df = df.dropna()
            X = _X[index]

            if len(df) > 0:
                df['BagLabels'] = None
                df['BagName'] = None
                df['FileName'] = fileNames[index]

                window = 50*60*_window
                i = 0
                while i < len(X):
                    if i + window > (len(X)):
                        break

                    else:
                        minDatetime = np.min(X.values[i:i+window, (list(X.columns).index('datetime'))])
                        maxDatetime = np.max(X.values[i:i+window, (list(X.columns).index('datetime'))])


                        labels = list(X.values[i:i+window, (list(X.columns).index('y'))])
                        
                        GroupedLabels = list()
                        for everyLabel in labels:
                            if everyLabel == 'None':
                                GroupedLabels.append('Non-Agitated')
                            else:
                                GroupedLabels.append('Agitated')

                        CountPercentage = Counter(GroupedLabels)
                        total = sum(CountPercentage.values())
                        percent = {key: (value/total)*100 for key, value in CountPercentage.items()}
                        if 'Agitated' in percent:
                            _label = round(percent['Agitated'],-1)
                        else:
                            _label = 0.0

                        indexes = (df.loc[(df['datetime'] >= minDatetime) & (df['datetime'] < maxDatetime)]).index
                        df.loc[min(indexes): max(indexes), 'BagName'] = 'Bag' + str(bagCounter)
                        df.loc[min(indexes): max(indexes), 'BagLabels'] = _label
                        #print(str(minDatetime) + ' : ' + str(maxDatetime) + ' : ' + str(labels)+ ' : ' + str(min(indexes)) + ' : ' +  str(max(indexes)) )

                        i = (i + window)
                        bagCounter = bagCounter + 1

                df['datetime'] = pd.to_datetime(df['datetime'])
                df['datetime'] = (df['datetime'] - dt.datetime(1970,1,1)).dt.total_seconds()
                df = df.dropna() #Drop all None
                allDfs.append(df)

            else:
                pass
                #logging.info('File is null - Might contain nulls : ' + str(fileNames[index]))

        if len(allDfs) > 0:
            cDf = pd.concat(allDfs)
            return cDf
        else:
            return None

    def CreateBagsOfInstances(self, df, colToRemove = None, model = None):
        if colToRemove != None:
            df = df.drop(colToRemove, axis = 'columns')
        bags = list(df['BagName'].unique())
        
        instances = list()
        instances_df = list()
        label = list()

        col = list(df.columns)
        if 'BagName' in col:
            col.remove('BagName')
        if 'BagLabels' in col:
            col.remove('BagLabels')        
        if 'y' in col:
            col.remove('y')        
        if 'FileName' in col:
            col.remove('FileName')                
        for everyBag in bags:

            filterData = df.loc[df['BagName'] == everyBag].reset_index(drop = True)
            if model == None:
                label.append(filterData.iloc[0]['BagLabels'])
                filterData = filterData[col]
                instances.append(np.array(filterData).reshape(1,-1))
            else:# model == 'SVM':
                X, y = self.CreateStackedDataSetsMIL(filterData)
                label.append(y)
                filterData = filterData[col]
                instances.append(X)
            instances_df.append(filterData)
        
        if model == None:
            instances = np.concatenate(instances, axis = 0)
        return bags, instances, label, instances_df

class Evaluation():
    timestrTxt = ''
    dfPath = ''
    resultForDf = list()
    
    def __init__(self, pid, destination, model, window, overlap):
        self.timestrTxt = self.CreatePathIfNotExists(destination + '/' + pid ) + '/' + (time.strftime("%Y%m%d") + '_' + pid + '_' + model + '_' + str(window) + '_' + str(overlap) + '.txt')
        self.dfPath = destination + (time.strftime("%Y%m%d") + '_Regression_CompleteResults.csv')
        open(self.timestrTxt, 'w').close()
        
        self.resultForDf = list()
        if os.path.isfile(self.dfPath):
            pass
        else:
            df = pd.DataFrame(columns = ['Subject', 'Model', 'AccuracyTrain', 'AccuracyTest', 'AccuracyAvg', 'RSquare', 'Train-Test-Split', 'Window', 'AnnotationWindow'])
            df.to_csv(self.dfPath, index=False)

    def CreatePathIfNotExists(self, path):
        if not os.path.exists(path):
            os.makedirs(path)
            return path
        else:
            return path

    def SaveResult(self, txt):
        result = open(self.timestrTxt,'a')
        result.write(txt + '\n')
        result.close()
    
    def AppendResult(self, txt):
        self.resultForDf.append(txt)
    
    def CombinedResultAsDf(self):
        df = pd.read_csv(self.dfPath)
        df.loc[len(df)] = self.resultForDf
        df.to_csv(self.dfPath, index=False)
        
class HandlingDirectories():
    def CreatePathIfNotExists(self, path):
        if not os.path.exists(path):
            os.makedirs(path)
            return path
        else:
            return path
        
    def ReadFiles(self, pid, pathToFiles, window, overlap):
            p_id = pid
            p = {}
            directories = os.listdir(pathToFiles)

            fdirectories = []
            for d in directories:
                if d == 'main.py' or d == 'run.sh' or d == '.ipynb_checkpoints':
                    pass
                else:
                    fdirectories.append(d)

            for everyDay in fdirectories:
                pwd = os.listdir(pathToFiles +everyDay)
                for everyPWD in pwd:
                    if(everyPWD == p_id):
                        path = pathToFiles + everyDay + '/' + everyPWD
                        allCSVs = os.listdir(path)
                        CSVName = str(window) + '_' + str(overlap) + '_' + everyPWD + '_' + everyDay + '.csv'
                        for everyCSV in allCSVs:
                            if(everyCSV == CSVName):
                                path = path+ '/' + everyCSV
                                try:
                                    file = pd.read_csv(path)
                                    if len(file) > 0:
                                        p[everyPWD + '_' + everyDay] = file
                                    else:
                                        raise Exception('File Read but is empty.' + str(CSVName))
                                except Exception as e:
                                    print(e)
                                    print('This file was not read: ' + path + '\nFile Size: ' + str(os.stat(path).st_size) + '\n')

            p = OrderedDict(sorted(p.items()))
            p_0 = list(p.values())
            p_fileName = list(p.keys())

            return p_0, p_fileName

def EqualizeFeatureInstances(dfToEqualize):
    result = (dfToEqualize.groupby(['BagName']).size()).reset_index()
    result.columns = ['BagName', 'Count']
    uniqueInstances = list(result['Count'].unique())
    
    if len(uniqueInstances) == 1:
        return dfToEqualize
    else:
        minInstances = min(uniqueInstances)
        BagList = list((result['BagName'].loc[result['Count'] == minInstances]).unique())
        
        if max(uniqueInstances) - minInstances == 1:
            #Add rows
            for everyBag in BagList:
                m = (dfToEqualize.loc[dfToEqualize['BagName'] == everyBag]).mean()
                m.columns = dfToEqualize.columns
                m['BagName'] = everyBag
                
                dfToEqualize = dfToEqualize.append(m, ignore_index = True)
                dfToEqualize = dfToEqualize.reset_index(drop = True)
            
            return dfToEqualize
        else:
            #Remove them
            return dfToEqualize[~dfToEqualize['BagName'].isin(BagList)]        
        
def PrepareTrainAndTestMIL(t, split = 0.3):
    
    BagOfInstances = BoI()
    
    uniqueBags = t['BagName'].unique()
    trainset, testset = train_test_split(uniqueBags, test_size=split)
    
    trainset = list(set(trainset.tolist()))
    trainset = t.loc[t['BagName'].isin(list(trainset))].reset_index(drop=True) 
    trainset = EqualizeFeatureInstances(trainset)
    
    testset = list(set(testset.tolist()))
    testset = t.loc[t['BagName'].isin(list(testset))].reset_index(drop=True)
    testset = EqualizeFeatureInstances(testset)

    Trainbags, TrainInstances, TrainLabel, df = BagOfInstances.CreateBagsOfInstances(trainset)
    Testbags, TestInstances, TestLabel, df = BagOfInstances.CreateBagsOfInstances(testset)
    
    return TrainInstances, TrainLabel, TestInstances, TestLabel

def AdaBoosting_Regression_MIL(df, split= 0.3):
    X_train, y_train, X_test, y_test = PrepareTrainAndTestMIL(df, split)
    #clf = AdaBoostRegressor(n_estimators=50,learning_rate=1,random_state=1)
    clf = AdaBoostRegressor(DecisionTreeRegressor(max_depth=4, min_samples_leaf=10), n_estimators=50,learning_rate=1)
    clf.fit(X_train,y_train)
    y_pred=clf.predict(X_test)
    
    results = {}
    results['TrainResult'] = np.mean(cross_val_score(clf, X_train, y_train, cv=5))
    results['MAE'] = metrics.mean_absolute_error(y_test, clf.predict(X_test))
    results['RSquare'] = metrics.r2_score(y_test, clf.predict(X_test))
    
    
    return results, clf

def PrepareData(df, colToRemove = None, model = None):
    bags = list(df['BagName'].unique())

    X = list()
    y = list()

    col = list(df.columns)
    if 'BagName' in col:
        col.remove('BagName')
    if 'BagLabels' in col:
        col.remove('BagLabels')        
    if 'y' in col:
        col.remove('y')        
    if 'FileName' in col:
        col.remove('FileName')                
    for everyBag in bags:

        filterData = df.loc[df['BagName'] == everyBag].reset_index(drop = True)
        y.append(filterData.iloc[0]['BagLabels'])
        filterData = filterData[col]
        if model == None:
            X.append(np.array(filterData).reshape(1,-1))
        else:
            X.append(np.array(filterData))
        #instances_df.append(filterData)

    if model == None:
        X = np.concatenate(X, axis = 0)
    return X, y

def GenerateImages(destination, pid, BaggedData, clf, key):
    _ReadDirectory = HandlingDirectories()
    _BagOfInstances = BoI()
    
    _ReadDirectory.CreatePathIfNotExists(destination + '060_Results/' + 'RegressionImages/')
    
    bags, X, y, instances_df = _BagOfInstances.CreateBagsOfInstances(BaggedData)
    y_pred=clf.predict(X)
    
    fig = plt.figure()
    x_ax = range(len(y))
    plt.scatter(x_ax, y, s=5, color="blue", label="original")
    plt.plot(x_ax, y_pred, lw=0.8, color="red", label="predicted")
    plt.legend()
    plt.title(pid + '-' + key)
    plt.xlabel("# of Bag")
    plt.ylabel("Predicted Percentage")
    plt.rcParams["figure.figsize"] = [8,6]
    plt.savefig(destination + '060_Results/' + 'RegressionImages/' + pid + '_'+ key + '.png')

def Evaluate(dfs, Xs, FileNames, _evaal, windowOfAnnotations, clf, destination):
    _BagOfInstances = BoI()
    _ReadDirectory = HandlingDirectories()
    
    fileNumber=  list()
    TrainAccuracy = {}
    Distribution = {} 
    
    for f in range(0, len(dfs)):
        df = dfs[f]
        X = Xs[f]
        file = FileNames[f]
        try:
            BaggedData = _BagOfInstances.BagLabel([df], [X], [file], windowOfAnnotations)
            BaggedData = BaggedData.reset_index(drop=True)

            Distribution[file] = str(dict(Counter(list(X['y']))))
            bags, X, y, instances_df = _BagOfInstances.CreateBagsOfInstances(BaggedData)
            y_pred=clf.predict(X)
            TrainAccuracy[file] = metrics.mean_absolute_error(y, clf.predict(X))
        except Exception as e:
            logging.info(str(file) + ' : ' + str(e)) 
            break
    
    Accuracy = list()
    for d in TrainAccuracy.keys():
        logging.info(d + ' : ' + str(TrainAccuracy[d])+ ' : ' + Distribution[d])
        _evaal.SaveResult(d + ' : ' +  str(TrainAccuracy[d]) + ' : ' + Distribution[d])
        Accuracy.append(TrainAccuracy[d])
    
    logging.info('Avg Accuracy (Training Files): ' + str(np.mean(Accuracy)))
    _evaal.SaveResult('Avg Accuracy (Training Files): ' + str(np.mean(Accuracy)))
    _evaal.AppendResult(np.mean(Accuracy))


def Prepare(pathToBes, pathToFes, pwd, window, overlap, windowOfAnnotations, destination, _train_test_split):
    logging.info('Check for Multiple pids...')
    pwd = pwd.split(';')
    logging.info('Total pwds: %s', len(pwd))
    
    ReadDirectory = HandlingDirectories()
    BagOfInstances = BoI()

    for p_id in pwd:
        _evaal = Evaluation(p_id, destination + '060_Results/', 'AdabosstRegression', window, overlap)
        _evaal.SaveResult('Date: ' + time.strftime("%Y%m%d"))
        _evaal.SaveResult('Subject: ' + p_id)
        _evaal.SaveResult('Window (in rows): ' +  str(window))
        _evaal.SaveResult('Overlap: ' + str(overlap))
        _evaal.SaveResult('Window of Annotations: ' + str(windowOfAnnotations))
        _evaal.SaveResult('Train-test split: ' + str(_train_test_split))

        logging.info('Classifer for ' + p_id)
        
        dfs, FileNames = ReadDirectory.ReadFiles(p_id, pathToFes, window, overlap)
        logging.info('Files Read: %s', str(len(dfs)))
        logging.info('Files FEs %s', ','.join(FileNames))
        Xs, FileNames = ReadDirectory.ReadFiles(p_id, pathToBes, window, overlap)
        logging.info('Files Read: %s', str(len(Xs)))
        logging.info('Files BFs %s', ','.join(FileNames))        

        BaggedData = BagOfInstances.BagLabel(dfs, Xs, FileNames, windowOfAnnotations)
        
        results, clf = AdaBoosting_Regression_MIL(BaggedData, _train_test_split)
        GenerateImages(destination, p_id, BaggedData, clf, str(window) + '_' + str(windowOfAnnotations))
        
        _evaal.SaveResult('\n\n' + str('Classification Report') + '\n' + str(results))
        _evaal.AppendResult(p_id)
        _evaal.AppendResult('AdabosstRegression')
        _evaal.AppendResult(results['TrainResult'])
        _evaal.AppendResult(results['MAE'])
        
        Evaluate(dfs, Xs, FileNames, _evaal, windowOfAnnotations, clf, destination)
        _evaal.AppendResult(results['RSquare'])
        _evaal.AppendResult(_train_test_split)
        _evaal.AppendResult(window)
        _evaal.AppendResult(windowOfAnnotations)
        _evaal.CombinedResultAsDf()

if __name__ == "__main__": 
    parser = argparse.ArgumentParser()
    parser.add_argument("source_fes", help="Path to read files.",type=str)
    parser.add_argument("source_bfs", help="Path to read files.",type=str)
    parser.add_argument("destination", help="Path to save files.",type=str)
    parser.add_argument("pwdid", help="Person with dementia id",type=str)
    parser.add_argument("window", help="Feature extraction window, in seconds.",type=int, default=60)
    parser.add_argument("overlap", help="Overlap of windows, in percentage (no percentage sign).",type=int,default=50)
    parser.add_argument("window_annotations", help="Annotations window, in minutes.",type=int,default=20)
    parser.add_argument("_train_test_split", help="train_test_split, in percentage (no percentage sign).",type=int,default=30)
    
    args = parser.parse_args()
    
    source_fes = args.source_fes 
    source_bfs = args.source_bfs 
    destination = args.destination
    p_id = args.pwdid
    window = args.window
    overlap = args.overlap
    aWindow = args.window_annotations
    _train_test_split = args._train_test_split
                    
    logging.basicConfig(filename= destination + '/090_Logging' + '/Adabosst_Regression_'+str(window)+'_'+str(overlap)+'_'+str(aWindow)+'.log', level=logging.INFO, filemode="w")
    logging.info('Argument read...:')
    logging.info('Source FEs: %s', str(source_fes))
    logging.info('Source BEs: %s', str(source_bfs))
    logging.info('Destination: %s', str(destination))
    logging.info('Subject: %s', str(p_id))
    logging.info('Window (in rows): %s', str(window))
    logging.info('Overlap: %s', str(overlap) + '%')
    logging.info('Window Annotations: %s', str(aWindow) + '%')
    logging.info('Train-test split: %s', str(_train_test_split) + '%')
    
    Prepare(source_bfs, source_fes, p_id, int(window)*50, overlap, aWindow, destination, float(_train_test_split/100))