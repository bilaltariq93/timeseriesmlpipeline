# 1. source
# 2. destination
# 3. pwd
# 4. window (In seconds) (Optional)
# 5. overlap (In pecentage) (Optional)
# 6. window of annotations (in minutes) (Optional)
# 7. _train_test_split (in percentage) (Optional, for analysis purpose)

set _sourceFEs = "/mnt/mmis-insidedem/DataBT/ThesisPrototype_4/040_FeatureExtracted/"
set _sourceBFs = "/mnt/mmis-insidedem/DataBT/ThesisPrototype_4/020_BandPassFilter/"
set _destination = "/mnt/mmis-insidedem/DataBT/ThesisPrototype_7/"
set _pwd = "X110;X111;X113;X114;X121;X122;X124;X126";
set _window = 60
set _overlap = 50
set _windowOfAnnotations = 20
set _train_test_split = 30

python3 AdaBoostRegressor.py $_sourceFEs $_sourceBFs $_destination $_pwd $_window $_overlap $_windowOfAnnotations $_train_test_split
