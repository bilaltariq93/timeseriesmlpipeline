# 1. source
# 2. destination
# 3. pwd
# 4. window (In seconds) (Optional)
# 5. overlap (In pecentage) (Optional)

set _source = "/mnt/mmis-insidedem/DataMP/040_data_restructured/"
set _destination = "/mnt/mmis-insidedem/DataBT/ThesisPrototype_1/"
set _pwd = "X110"
set _window = 60
set _overlap = 50

python3 main.py $_source $_destination $_pwd $_window $_overlap