# 1. source
# 2. destination
# 3. pwd
# 4. window (In seconds) (Optional)
# 5. overlap (In pecentage) (Optional)
# 6. window of annotations (in minutes) (Optional)
# 7. _train_test_split (in percentage) (Optional, for analysis purpose)
# 8. Undersampling (Optional, for analysis purpose)
# 9. _type_Annotation (Video / Live)

set _source = "/mnt/mmis-insidedem/DataMP/040_data_restructured/"
set _destination = "/mnt/mmis-insidedem/DataBT/ThesisPrototype_1/"
set _pwd = "X114;X121;X122;X124;X126"
set _window = 60
set _overlap = 50
set _windowOfAnnotations = 20
set _train_test_split = 30
set _undersampling = 0 #True
set _type_Annotation = 'Live'

python3 master.py $_source $_destination $_pwd $_window $_overlap $_windowOfAnnotations $_train_test_split $_undersampling $_type_Annotation
