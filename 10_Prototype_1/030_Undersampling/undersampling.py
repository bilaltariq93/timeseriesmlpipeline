import pandas as pd
import numpy as np
from imblearn.under_sampling import RandomUnderSampler
from collections import Counter

class UnderSampling():
    def __init__(self):
        pass
    
    def BinaryClassification(self, row):
        if row['y'] == 'None' or row['y'] == 'none' or row['y'] == 'NV':
            val = 0
        else:
            val = 1

        return val
    
    def UnderSampledData(self, df):
        orignalDist = str(dict(Counter(df['y'])))
        if len((dict(Counter(df['y']))).keys()) <= 1:
            return df, orignalDist, str('Undersampling not applied')
        
        df['y1'] = df.apply(self.BinaryClassification, axis=1)
        
        X = df[['datetime', 'x1', 'x2', 'x3', 'y']]
        y = df[['y1']]
        
        distOrignal = dict(Counter(y['y1']))
        dist = distOrignal.copy()
        if int(dist[0] * 0.25) > distOrignal[1]: #Undersample Non Agitated Behaviour
            dist[0] = int((dist[1] * 0.75) / 0.25)
        else:
            dist[1] = int(dist[0] * 0.25) # #Undersample Agitated Behaviour

        undersample = RandomUnderSampler(random_state=42,sampling_strategy= dist)
        undersample.fit(X, y)
        X_resampled, y_resampled = undersample.fit_resample(X, y)
        y_resampled = y_resampled.reset_index(drop=False)
        X_resampled = X_resampled.reset_index(drop=False)
        df = X_resampled.merge(y_resampled, left_index=True, right_index=True)
        df = df.sort_values('datetime').drop(['index_x', 'index_y'], axis = 1).reset_index(drop = True)
        
        df = df[['datetime', 'x1', 'x2', 'x3', 'y']]
        undersampledDist = str(dict(Counter(df['y'])))

        return df, orignalDist, undersampledDist
