import pandas as pd
from datetime import datetime, timedelta
import os
import datetime as dt
import numpy as np
import time
from collections import OrderedDict
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report,confusion_matrix
from imblearn.over_sampling import RandomOverSampler
from imblearn.under_sampling import RandomUnderSampler
from mil.preprocessing import StandarizerBagsList
from mil.bag_representation import MILESMapping
from imblearn.over_sampling import SMOTE
from mil.models import RandomForestClassifier, SVC
from mil.models import SVC
from mil.trainer import Trainer
from mil.metrics import AUC
from mil.validators import KFold
from mil.metrics import Specificity
from mil.metrics import TrueNegatives
from mil.metrics import TruePositives
from mil.metrics import FalseNegatives
from mil.metrics import FalsePositives
from mil.metrics import Precision
from mil.metrics import Recall
from mil.metrics import AUC
from sklearn.metrics import roc_curve,roc_auc_score
from sklearn.metrics import auc
from mil.models import APR
pd.options.display.float_format = '{:20.20}'.format
from sklearn import preprocessing
import sys
import random
from sklearn.ensemble import AdaBoostClassifier
from sklearn import metrics
from sklearn.model_selection import cross_val_score
import logging
import argparse
from scipy.signal import butter
from scipy.signal import filtfilt
from scipy.signal import medfilt
import pandas as pd
import numpy as np
import os 
from collections import OrderedDict
import time
from scipy.stats import kurtosis, skew
from scipy.stats import iqr
import datetime
import copy as cp 
import math
from scipy.signal import welch
import argparse
import traceback

class FeatureExtracted():
    def __init__(self):
        pass
    
    def CreatePathIfNotExists(self, path):
        if not os.path.exists(path):
            os.makedirs(path)
            return path
        else:
            return path

    def TeagerEnergy(self, signal):
        Teager_power=np.zeros(len(signal))
        for i in range(1,len(signal)-1):
            Teager_power[i]=signal[i]*signal[i]-signal[i+1]*signal[i-1]
        return Teager_power


    def GetTeagerCalculatedDf(self, X):
        cmyDf = X.copy()
        cols = cmyDf.columns
        for c in range(1,cmyDf.shape[1]-2):
            cmyDf[cols[c]] = self.TeagerEnergy(cmyDf[cols[c]])
        return cmyDf

    def FeatureExtraction_Optimized(self, X, window, overlap, featureToExtract, windowedIndexes):
        X = X.reset_index(drop=True)

        t0 = time.time()

        if len(featureToExtract) == 0:
            return 'No Feature Provided'

        cols= list()
        cols.append('datetime')
        for f in featureToExtract:
            for col in X.columns:
                if col == 'datetime':
                    pass
                elif col == 'y':
                    pass
                elif col == 'y1':
                    pass
                else:
                    cols.append(str(col) + '_' + f)
        cols.append('x1_x2_corr')
        cols.append('x2_x3_corr')
        cols.append('x1_x3_corr')

        cmyDf = pd.DataFrame(columns=cols)

        t_0_0 = time.time()
        tkdf = self.GetTeagerCalculatedDf(X)
        t_0_1 = time.time()
        #print('Time take for Teager Energy : ' + str(t_0_1-t_0_0))

        for k, v in windowedIndexes.items():


            wResult = list()
            indexes = v.split(':')

            mean_1, median_1, max_1, std_1, var_1, rms_1, skewness_1, kurtosis_1, iqr_1 = self.MasterFeatureExtraction(np.array(X.values[int(indexes[0]):int(indexes[1]),1]))
            mean_2, median_2, max_2, std_2, var_2, rms_2, skewness_2, kurtosis_2, iqr_2 = self.MasterFeatureExtraction(np.array(X.values[int(indexes[0]):int(indexes[1]),2]))
            mean_3, median_3, max_3, std_3, var_3, rms_3, skewness_3, kurtosis_3, iqr_3 = self.MasterFeatureExtraction(np.array(X.values[int(indexes[0]):int(indexes[1]),3]))

            t_mean_1, t_max_1, t_variance_1 = self.MasterTeagerEnergy(np.array(tkdf.values[int(indexes[0]):int(indexes[1]),1]))
            t_mean_2, t_max_2, t_variance_2 = self.MasterTeagerEnergy(np.array(tkdf.values[int(indexes[0]):int(indexes[1]),2]))
            t_mean_3, t_max_3, t_variance_3 = self.MasterTeagerEnergy(np.array(tkdf.values[int(indexes[0]):int(indexes[1]),3]))

            x1_x2, x2_x3, x1_x3 = self.MasterCorrelation(np.array(X.values[int(indexes[0]):int(indexes[1]),1:4]))


            psd_0_1_to_0_5_mean_1, psd_0_5_to_3_mean_1, psd_3_and_above_mean_1, psd_0_1_to_0_5_max_1, psd_0_5_to_3_max_1, psd_3_and_above_max_1, mrc_0_1_to_0_5_1, mrc_0_5_to_3_1, mrc_3_and_above_1, zrc_0_1_to_0_5_1, zrc_0_5_to_3_1, zrc_3_and_above_1 = self.MasterPowerDensitySpecturm(np.array(X.values[int(indexes[0]):int(indexes[1]),1]))
            psd_0_1_to_0_5_mean_2, psd_0_5_to_3_mean_2, psd_3_and_above_mean_2, psd_0_1_to_0_5_max_2, psd_0_5_to_3_max_2, psd_3_and_above_max_2, mrc_0_1_to_0_5_2, mrc_0_5_to_3_2, mrc_3_and_above_2, zrc_0_1_to_0_5_2, zrc_0_5_to_3_2, zrc_3_and_above_2 = self.MasterPowerDensitySpecturm(np.array(X.values[int(indexes[0]):int(indexes[1]),2]))
            psd_0_1_to_0_5_mean_3, psd_0_5_to_3_mean_3, psd_3_and_above_mean_3, psd_0_1_to_0_5_max_3, psd_0_5_to_3_max_3, psd_3_and_above_max_3, mrc_0_1_to_0_5_3, mrc_0_5_to_3_3, mrc_3_and_above_3, zrc_0_1_to_0_5_3, zrc_0_5_to_3_3, zrc_3_and_above_3 = self.MasterPowerDensitySpecturm(np.array(X.values[int(indexes[0]):int(indexes[1]),3]))

            wResult.append(X.iloc[int(indexes[0])]['datetime'])

            #Stats#
            wResult.append(mean_1)
            wResult.append(mean_2)
            wResult.append(mean_3)
            wResult.append(median_1)
            wResult.append(median_2)
            wResult.append(median_3)
            wResult.append(max_1)
            wResult.append(max_2)
            wResult.append(max_3)
            wResult.append(std_1)
            wResult.append(std_2)
            wResult.append(std_3)
            wResult.append(var_1)
            wResult.append(var_2)
            wResult.append(var_3)
            wResult.append(rms_1)
            wResult.append(rms_2)
            wResult.append(rms_3)
            wResult.append(skewness_1)
            wResult.append(skewness_2)
            wResult.append(skewness_3)
            wResult.append(kurtosis_1)
            wResult.append(kurtosis_2)
            wResult.append(kurtosis_3)
            wResult.append(iqr_1)
            wResult.append(iqr_2)
            wResult.append(iqr_3)
            #Stats#

            #Teager#
            wResult.append(t_mean_1)
            wResult.append(t_mean_2)
            wResult.append(t_mean_3)
            wResult.append(t_max_1)
            wResult.append(t_max_2)
            wResult.append(t_max_3)
            wResult.append(t_variance_1)
            wResult.append(t_variance_2)
            wResult.append(t_variance_3)
            #Teager#

                    #PSD#
            wResult.append(psd_0_1_to_0_5_mean_1)
            wResult.append(psd_0_1_to_0_5_mean_2)
            wResult.append(psd_0_1_to_0_5_mean_3)

            wResult.append(psd_0_5_to_3_mean_1)
            wResult.append(psd_0_5_to_3_mean_2)
            wResult.append(psd_0_5_to_3_mean_3)

            wResult.append(psd_3_and_above_mean_1)
            wResult.append(psd_3_and_above_mean_2)
            wResult.append(psd_3_and_above_mean_3)

            wResult.append(psd_0_1_to_0_5_max_1)
            wResult.append(psd_0_1_to_0_5_max_2)
            wResult.append(psd_0_1_to_0_5_max_3)

            wResult.append(psd_0_5_to_3_max_1)
            wResult.append(psd_0_5_to_3_max_2)
            wResult.append(psd_0_5_to_3_max_3)

            wResult.append(psd_3_and_above_max_1)
            wResult.append(psd_3_and_above_max_2)
            wResult.append(psd_3_and_above_max_3)

            wResult.append(mrc_0_1_to_0_5_1)
            wResult.append(mrc_0_1_to_0_5_2)
            wResult.append(mrc_0_1_to_0_5_3)

            wResult.append(mrc_0_5_to_3_1)
            wResult.append(mrc_0_5_to_3_2)
            wResult.append(mrc_0_5_to_3_3)

            wResult.append(mrc_3_and_above_1)
            wResult.append(mrc_3_and_above_2)
            wResult.append(mrc_3_and_above_3)


            wResult.append(zrc_0_1_to_0_5_1)
            wResult.append(zrc_0_1_to_0_5_2)
            wResult.append(zrc_0_1_to_0_5_3)

            wResult.append(zrc_0_5_to_3_1)
            wResult.append(zrc_0_5_to_3_2)
            wResult.append(zrc_0_5_to_3_3)

            wResult.append(zrc_3_and_above_1)
            wResult.append(zrc_3_and_above_2)
            wResult.append(zrc_3_and_above_3)
            #PSD#

            #corr#
            wResult.append(x1_x2)
            wResult.append(x2_x3)
            wResult.append(x1_x3)
            #corr#

            cmyDf.loc[len(cmyDf)] = wResult


        t1 = time.time()

        print('Time take for FEs ' + str(t1-t0))
        return cmyDf

    def CalculateMean(self, X):
        return np.mean(X)

    def CalculateMedian(self, X):
        return np.median(X)

    def CalculateMax(self, X):
        return np.max(X)

    def CalculateStd(self, X):
        return np.std(X)

    def CalculateVariance(self, X):
        return np.var(X)

    def CalculateRMS(self, X):
        return np.sqrt(np.mean(X ** 2))

    def CalculateSkewness(self, X):
        return skew(X)

    def CalculateKurtosis(self, X):
        return kurtosis(X)

    def CalculateIqr(self, X):
        return iqr(X)

    def MasterFeatureExtraction(self, X):
        _mean = self.CalculateMean(X)
        _median = self.CalculateMedian(X)
        _max = self.CalculateMax(X)
        _std = self.CalculateStd(X)
        _var = self.CalculateVariance(X)
        _rms = self.CalculateRMS(X)
        _skewness = self.CalculateSkewness(X)
        _kurtosis = self.CalculateKurtosis(X)
        _iqr = self.CalculateIqr(X)

        return _mean, _median, _max, _std, _var, _rms, _skewness, _kurtosis, _iqr

    def MasterTeagerEnergy(self, X):
        _mean = self.CalculateMean(X)
        _var = self.CalculateVariance(X)
        _max = self.CalculateMax(X)

        return _mean, _max, _var

    def MasterCorrelation(self, X):
        t = pd.DataFrame(X)
        t = t.astype(float) 
        _t = t.corr()

        x1_x2 = _t.iloc[0][1]
        x1_x3 = _t.iloc[0][2]
        x2_x3 = _t.iloc[1][2]

        return x1_x2, x2_x3, x1_x3

    def PowerDensitySpecturm(self, X):
        arr = X.astype(np.float)
        #wfreqs, wpsd = welch(arr,fs=50, nperseg=500, noverlap=(500 // 2),detrend=None, scaling='density')
        wpsd = np.abs(np.fft.fft(arr))**2
        wfreqs = np.fft.fftfreq(len(wpsd), 0.02)
        myDf = pd.DataFrame({'freq' : wfreqs,'psd' : wpsd})
        return myDf

    def PowerDensitySpecturmRange(self, df, xmin, xmax):
        if xmax == None:
            return df.loc[(df['freq'] > xmin)]
        else:
            return df.loc[((df['freq'] > xmin) & (df['freq'] <= xmax))]

    def MasterPowerDensitySpecturm(self, X):
        _df = self.PowerDensitySpecturm(X)

        psd_0_1_to_0_5_mean  = np.mean(list(self.PowerDensitySpecturmRange(_df, 0.1, 0.5)['psd']))
        psd_0_5_to_3_mean  = np.mean(list(self.PowerDensitySpecturmRange(_df, 0.5, 3)['psd']))
        psd_3_and_above_mean  = np.mean(list(self.PowerDensitySpecturmRange(_df, 3, None)['psd']))

        psd_0_1_to_0_5_max  = np.max(list(self.PowerDensitySpecturmRange(_df, 0.1, 0.5)['psd']))
        psd_0_5_to_3_max = np.max(list(self.PowerDensitySpecturmRange(_df, 0.5, 3)['psd']))
        psd_3_and_above_max  = np.max(list(self.PowerDensitySpecturmRange(_df, 3, None)['psd']))

        mrc_0_1_to_0_5  = (np.nonzero(np.diff(np.array(self.PowerDensitySpecturmRange(_df, 0.1, 0.5)['psd']) > np.mean(np.array(self.PowerDensitySpecturmRange(_df, 0.1, 0.5)['psd']))))[0]).size
        mrc_0_5_to_3  = (np.nonzero(np.diff(np.array(self.PowerDensitySpecturmRange(_df, 0.5, 3)['psd']) > np.mean(np.array(self.PowerDensitySpecturmRange(_df, 0.5, 3)['psd']))))[0]).size
        mrc_3_and_above  = (np.nonzero(np.diff(np.array(self.PowerDensitySpecturmRange(_df, 3, None)['psd']) > np.mean(np.array(self.PowerDensitySpecturmRange(_df, 3, None)['psd']))))[0]).size

        zrc_0_1_to_0_5 = (np.nonzero(np.diff(np.array(self.PowerDensitySpecturmRange(_df, 0.1, 0.5)['psd']) > 0))[0]).size
        zrc_0_5_to_3 = (np.nonzero(np.diff(np.array(self.PowerDensitySpecturmRange(_df, 0.1, 0.5)['psd']) > 0))[0]).size
        zrc_3_and_above = (np.nonzero(np.diff(np.array(self.PowerDensitySpecturmRange(_df, 0.1, 0.5)['psd']) > 0))[0]).size

        return psd_0_1_to_0_5_mean, psd_0_5_to_3_mean, psd_3_and_above_mean, psd_0_1_to_0_5_max, psd_0_5_to_3_max, psd_3_and_above_max, mrc_0_1_to_0_5, mrc_0_5_to_3, mrc_3_and_above, zrc_0_1_to_0_5, zrc_0_5_to_3, zrc_3_and_above

    def ReadFiles(self, pid, pathToReadFiles, window, overlap):
        p_id = pid
        p = {}

        pathToFiles = pathToReadFiles
        directories = os.listdir(pathToFiles)
        fdirectories = []
        for d in directories:
            if d == 'main.py' or d == 'run.sh' or d == '.ipynb_checkpoints':
                pass
            else:
                fdirectories.append(d)    
        for everyDay in fdirectories:
            pwd = os.listdir(pathToFiles +everyDay)
            for everyPWD in pwd:
                if(everyPWD == p_id):
                    path = pathToFiles + everyDay + '/' + everyPWD
                    allCSVs = os.listdir(path)
                    CSVName = str(window) + '_' + str(overlap) + '_' + everyPWD + '_' + everyDay + '.csv'
                    for everyCSV in allCSVs:
                        if(everyCSV == CSVName):
                            path = path+ '/' + everyCSV
                            try:
                                file = pd.read_csv(path)
                                if len(file) > 0:
                                    p[everyPWD + '_' + everyDay] = path
                                else:
                                    raise Exception('File Read but is empty.' + str(CSVName))
                            except:
                                print('This file was not read: ' + path + '\nFile Size: ' + str(os.stat(path).st_size) + '\n')

        p = OrderedDict(sorted(p.items()))
        p_0 = list(p.values())
        p_fileName = list(p.keys())

        return p_0, p_fileName

    def CreateWindowedIndexes(self, p_0, window, overlap):
        t0 = time.time()
        #_ListOfWindowIndexes = []
        _window = window
        _overlap = overlap

        _e = p_0
        #for _e in p_0:
        _windowIndexes = {}
        _i = 0
        _index = 0
        while _i <  len(_e):
            if _i + _window > len(_e):
                _windowIndexes[_index] = str(_i) + ":" + str(len(_e))
                _i = len(_e)
            else:
                _windowIndexes[_index] = str(_i) + ":" + str(_i+_window)
                _index = _index + 1
                _i = _i + (math.floor((_overlap / 100) * _window))
            #_ListOfWindowIndexes.append(_windowIndexes)
        t1 = time.time()
        #print('Time take to create indexes for window: ' + str(t1-t0))
        return _windowIndexes

    def Worker(self, p_0, pathFe, window, overlap, fileName, p_id):
        #ListOfWindowIndexes = self.CreateWindowedIndexes(p_0, window, overlap)

        for index in range(0,len(p_0)):
            logging.info('Current File: %s', str(fileName[index]))
            listOfFes = ['mean', 'median', 'max', 'std', 'var', 'rms', 'skewness', 'kurtosis', 'iqr', 'tegear_mean', 'teager_max',  'teager_variance' ,'psd_mean_0_1_to_0_5', 'psd_mean_0_5_to_3','psd_mean_3_and_more', 'psd_max_0_1_to_0_5', 'psd_max_0_5_to_3','psd_max_3_and_more', 'mrc_0_1_to_0_5', 'mrc_0_5_to_3', 'mrc_3_and_more', 'zrc_0_1_to_0_5', 'zrc_0_5_to_3', 'zrc_3_and_more']
            try:
                _Fe = pd.read_csv(p_0[index])
                #_Fe = _Fe[['datetime', 'x1', 'x2', 'x3']]
                _Fe = self.FeatureExtraction_Optimized(_Fe, window, overlap, listOfFes, self.CreateWindowedIndexes(_Fe, window, overlap))
                self.CreatePathIfNotExists(pathFe[index])
                logging.info('Path created for file: %s', str(pathFe[index]))
                _Fe.to_csv(pathFe[index]+ (str(window) + "_" + str(overlap) + "_" + fileName[index] + '.csv'), index=False)
                logging.info('File saved as: %s', str(pathFe[index]+ (str(window) + "_" + str(overlap) + "_" + fileName[index])+'.csv'))

            except Exception as e:
                logging.info('Error in File: ' + str(fileName[index]))
                logging.info(e)
                logging.info(traceback.format_exc())

    def Prepare(self, source, destination, pwd, window, overlap):
        logging.info('Check for Multiple pids...')
        pwd = pwd.split(';')
        logging.info('Total pwds: %s', len(pwd))

        for p_id in pwd:
            logging.info('Feature extraction starting for' + p_id)
            logging.info('File Reading starting')

            p_0, fileName = self.ReadFiles(p_id, source, window, overlap)

            logging.info('Files Read: %s', str(len(p_0)))

            fileDate = [i.split('_', 1)[-1] for i in fileName]

            rootPathToFe = destination + '/040_FeatureExtracted'

            logging.info('Path Created: %s', str(rootPathToFe))

            pathFe = list()

            for fd in range(0, len(fileDate)):
                pathFe.append(rootPathToFe + '/' + fileDate[fd] + '/' + p_id + '/')

            self.Worker(p_0, pathFe, window, overlap, fileName, p_id)
