import sys
import logging
import argparse
from Featureextracted import FeatureExtracted 

def main(source, destination, pwd, window, overlap):
    fe = FeatureExtracted()
    fe.Prepare(source, destination, pwd, window, overlap)

if __name__ == "__main__": 
    
    parser = argparse.ArgumentParser()
    parser.add_argument("source", help="Path to read files.",type=str)
    parser.add_argument("destination", help="Path to save files.",type=str)
    parser.add_argument("pwdid", help="Person with dementia id",type=str)
    parser.add_argument("window", help="Feature extraction window, in seconds.",type=int, default=60)
    parser.add_argument("overlap", help="Overlap of windows, in percentage (no percentage sign).",type=int,default=50)
    
    args = parser.parse_args()
    
    source = args.source 
    destination = args.destination
                 
    p_id = args.pwdid
    window = args.window
    overlap = args.overlap
                    
    logging.basicConfig(filename= destination + '/FeatureExtracted.log', level=logging.INFO, filemode="w")
    logging.info('Argument read...:')
    logging.info('Source: %s', str(source))
    logging.info('Destination: %s', str(destination))
    logging.info('Subject: %s', str(p_id))
    logging.info('Window (in rows): %s', str(window))
    logging.info('Overlap: %s', str(overlap) + '%')
     
    main(source, destination, p_id, int(window)*50, overlap)