import os
import sys
import pandas as pd
import logging
import argparse
sys.path.append(os.getcwd() + '/010_MedianFilter/')
from Medianfilter import MedianFilter
sys.path.append(os.getcwd() + '/020_BandPassFilter/')
from Bandpassfilter import BandPassFilter
sys.path.append(os.getcwd() + '/040_FeatureExtracted/')
from Featureextracted import FeatureExtracted
sys.path.append(os.getcwd() + '/030_Undersampling/')
from undersampling import UnderSampling


def DataFilter(_df, _type):
    if _type == 'Live':
        df = _df[['DateTime', 'Accelerometer_Wrist_X', 'Accelerometer_Wrist_Y', 'Accelerometer_Wrist_Z', 'Live_Annotation']]
        logging.info('Orignal Length : ' +str(len(df)))
        df.columns = ['datetime', 'x1', 'x2', 'x3', 'y']
        df = df[df['y'] != 'Q']
        logging.info('Q removed : ' +str(len(df)))
        
    elif _type == 'Video':
        df = _df[['DateTime', 'Accelerometer_Wrist_X', 'Accelerometer_Wrist_Y', 'Accelerometer_Wrist_Z', 'Video_Annotation']]
        logging.info('Orignal Length : ' +str(len(df)))
        df.columns = ['datetime', 'x1', 'x2', 'x3', 'y']
        df = df[df['y'] != 'Q']
        logging.info('Q removed : ' +str(len(df)))
        
        df = df[df['y'] != 'None']
        logging.info('None removed : ' +str(len(df)))
        
        df = df[df['y'] != 'none']
        logging.info('none removed : ' +str(len(df)))        
        
        df = df[df['y'] != '#']
        logging.info('# removed : ' +str(len(df)))
        
        df = df[df['y'] != 'Person nicht sichtbar']
        logging.info('Person nicht sichtbar: removed : ' +str(len(df)))
        
        df = df[df['y'] != 'Person auf Video nicht sichtbar']
        logging.info('Person auf Video nicht sictbar: removed : ' +str(len(df)))
        
        df = df[df['y'] != 'Verhalten auf Video nicht sichtbar']
        logging.info('Verhalten auf Video nicht sichtbar removed : ' +str(len(df)))
    else:
        logging.info('Error: Annotation column invalid')
        exit(-1)
    
    df = df.dropna(subset=['x1', 'x2', 'x3'])
    logging.info('NA removed : ' +str(len(df)))
    df = df.reset_index(drop = True)
    
    return df
        
        

def main(source, destination, pid, window, overlap, aWindow, _train_test_split, unsample_status, _type_Annotation):
    
    #Median Filter Window
    if(window %2 ==0):
        window_mf = window-1
    else:
        window_mf = window
            
    #Precrossing Steps
    us = UnderSampling()
    mf = MedianFilter()
    bf = BandPassFilter()
    fe = FeatureExtracted()
    
    #List of FEs
    listOfFes = ['mean', 'median', 'max', 'std', 'var', 'rms', 'skewness', 'kurtosis', 'iqr', 'tegear_mean', 'teager_max',  'teager_variance' ,'psd_mean_0_1_to_0_5', 'psd_mean_0_5_to_3','psd_mean_3_and_more', 'psd_max_0_1_to_0_5', 'psd_max_0_5_to_3','psd_max_3_and_more', 'mrc_0_1_to_0_5', 'mrc_0_5_to_3', 'mrc_3_and_more', 'zrc_0_1_to_0_5', 'zrc_0_5_to_3', 'zrc_3_and_more']
    
    #Get the files
    p_0, fileName = mf.ReadFiles(pid, source)
    fileDate = [i.split('_', 1)[-1] for i in fileName]

    logging.info('Path where FEs will be saved')
    
    #Set the destination Path
    pathFe = list()
    for fd in range(0, len(fileDate)):
        logging.info(destination +  '040_FeatureExtracted/' + fileDate[fd] + '/' + pid + '/')
        pathFe.append(destination +  '040_FeatureExtracted/' + fileDate[fd] + '/' + pid + '/')

    #Set the labels Path
    pathBe = list()
    logging.info('\n\n Labels path.')
    for fd in range(0, len(fileDate)):
        logging.info(destination +  '020_BandPassFilter/' + fileDate[fd] + '/' + pid + '/')
        pathBe.append(destination +  '020_BandPassFilter/' + fileDate[fd] + '/' + pid + '/')

    #For each file the is read...
    logging.info('\nReading files')
    for index in range(0,len(p_0)):
        try:
            logging.info('\n'+str(p_0[index]))

            #Filtering data
            _mf = pd.read_csv(p_0[index])
            _mf = DataFilter(_mf, _type_Annotation)
            
            if len(_mf) > 0:
                
                ##Undersampling
                if unsample_status == 1:
                    _mf, orignalDist, undersampleDist = us.UnderSampledData(_mf)
                    logging.info('\tOrignal Annotation Distribution\t' + orignalDist)
                    logging.info('\tUndersampled Annotation Distribution\t' + undersampleDist)
                
                logging.info('\tApplying Median Filter...')

                if len(_mf) > window_mf: 
                    #Calculate the Median Filter
                    _mf = mf._MedianFilter(_mf, window_mf)

                    logging.info('\tApplying Bandpass Filter...')
                    #Calculate the Bandpass Filter
                    _bf = bf._BandPassFilter(_mf)
                    _bf_to_save = _bf.copy()
                    _bf_to_save = _bf_to_save[['datetime', 'y']]
                    fe.CreatePathIfNotExists(pathBe[index])
                    _bf_to_save.to_csv(pathBe[index]+ (str(window) + "_" + str(overlap) + "_" + fileName[index] + '.csv'), index=False)
                    logging.info('\tCalculating Features...')
                    #Calucalte the FEs and save it.
                    _Fe = fe.FeatureExtraction_Optimized(_bf, window, overlap, listOfFes, fe.CreateWindowedIndexes(_bf, window, overlap))

                    fe.CreatePathIfNotExists(pathFe[index])
                    logging.info('\tSaved features at ' + pathFe[index]+ (str(window) + "_" + str(overlap) + "_" + fileName[index] + '.csv'))
                    _Fe.to_csv(pathFe[index]+ (str(window) + "_" + str(overlap) + "_" + fileName[index] + '.csv'), index=False)
                else:
                    logging.info('File not processed. # of rows < Window size.' + str(p_0[index]) + ' : ' + str(len(_mf)))
        except Exception as e:
            print('Error in file' + str(p_0[index]))
            print(e)

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument("source", help="Path to read files.",type=str)
    parser.add_argument("destination", help="Path to save files.",type=str)
    parser.add_argument("pwdid", help="Person with dementia id",type=str)
    parser.add_argument("window", help="Feature extraction window, in seconds.",type=int, default=60)
    parser.add_argument("overlap", help="Overlap of windows, in percentage (no percentage sign).",type=int,default=50)
    parser.add_argument("window_annotations", help="Annotations window, in minutes.",type=int,default=20)
    parser.add_argument("_train_test_split", help="train_test_split, in percentage (no percentage sign).",type=int,default=30)
    parser.add_argument("unsample_status", help="unsample_status",type=int,default=1)
    parser.add_argument("_type_Annotation", help="_type_Annotation",type=str)
    
    args = parser.parse_args()
    
    source = args.source 
    destination = args.destination
                 
    p_id = args.pwdid
    window = args.window
    overlap = args.overlap
    aWindow = args.window_annotations
    _train_test_split = args._train_test_split
    unsample_status = args.unsample_status
    _type_Annotation = args._type_Annotation
    
    pwds = p_id.split(';')
    for pid in pwds:
        logging.basicConfig(filename= destination + '/Master_'+pid+'.log', level=logging.INFO, filemode="w")
        logging.info('Argument read...:')
        logging.info('Source: %s', str(source))
        logging.info('Destination: %s', str(destination))
        logging.info('Subject: %s', str(pid))
        logging.info('Window (in rows): %s', str(window))
        logging.info('Overlap: %s', str(overlap) + '%')
        logging.info('Window Annotations: %s', str(aWindow) + '%')
        logging.info('Train-test split: %s', str(_train_test_split) + '%')
        logging.info('unsample_status: %s', str(unsample_status))
        logging.info('_type_Annotation: %s', str(_type_Annotation))


        main(source, destination, pid, int(window)*50, overlap, aWindow, _train_test_split, unsample_status, _type_Annotation)
