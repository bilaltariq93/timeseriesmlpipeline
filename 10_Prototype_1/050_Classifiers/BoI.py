from libraries import *

class BoI():
    
    def __init__(self):
        pass
    
    def BagLabel(self, _df, _X, fileNames, _window, _isBinaryClassification = 1):
        bagCounter = 1
        allDfs = list()
        for index in range(0, len(_df)):
            df = _df[index]
            df = df.dropna()
            #print(fileNames[index] + " : " + str(len(df)))
            X = _X[index]

            if len(df) > 0:
                df['BagLabels'] = None
                df['BagName'] = None
                df['FileName'] = fileNames[index]

                window = 50*60*_window
                i = 0
                while i < len(X):
                    if i + window > (len(X)):
                        break

                    else:
                        minDatetime = np.min(X.values[i:i+window, (list(X.columns).index('datetime'))])
                        maxDatetime = np.max(X.values[i:i+window, (list(X.columns).index('datetime'))])


                        labels = list(X.values[i:i+window, (list(X.columns).index('y'))])
                        
                        if _isBinaryClassification == 1:
                            labels = set(labels)
                            #print(labels)
                            if len(labels) == 1 and ('None' in labels or 'NV' in labels):
                                _label = 0
                            else:
                                _label = 1
                        elif _isBinaryClassification == 0:
                            _dis = dict(Counter(labels))
                            v, k = max((v, k) for k, v in _dis.items())
                            _label = k
                            #print(str(_dis) + ' : ' + str(_label))
                            
                        else:
                            #regression
                            pass

                        indexes = (df.loc[(df['datetime'] >= minDatetime) & (df['datetime'] < maxDatetime)]).index
                        df.loc[min(indexes): max(indexes), 'BagName'] = 'Bag' + str(bagCounter)
                        df.loc[min(indexes): max(indexes), 'BagLabels'] = _label
                        #print(str(minDatetime) + ' : ' + str(maxDatetime) + ' : ' + str(labels)+ ' : ' + str(min(indexes)) + ' : ' +  str(max(indexes)) )

                        i = (i + window)
                        bagCounter = bagCounter + 1

                df['datetime'] = pd.to_datetime(df['datetime'])
                df['datetime'] = (df['datetime'] - dt.datetime(1970,1,1)).dt.total_seconds()
                df = df.dropna() #Drop all None
                allDfs.append(df)

            else:
                pass
                #logging.info('File is null - Might contain nulls : ' + str(fileNames[index]))

        if len(allDfs) > 0:
            cDf = pd.concat(allDfs)
            #print(cDf['BagLabels'].unique())
            return cDf
        else:
            return None

    def CreateBagsOfInstances(self, df, colToRemove = None, model = None):
        if colToRemove != None:
            df = df.drop(colToRemove, axis = 'columns')
        bags = list(df['BagName'].unique())
        
        instances = list()
        instances_df = list()
        label = list()

        col = list(df.columns)
        if 'BagName' in col:
            col.remove('BagName')
        if 'BagLabels' in col:
            col.remove('BagLabels')        
        if 'y' in col:
            col.remove('y')        
        if 'FileName' in col:
            col.remove('FileName')                
        for everyBag in bags:

#             filterData = df.loc[df['BagName'] == everyBag].reset_index(drop = True)
#             label.append(filterData.iloc[0]['BagLabels'])
#             filterData = filterData[col]
#             if model == None:
#                 instances.append(np.array(filterData).reshape(1,-1))
#             else:
#                 instances.append(np.array(filterData))
#             instances_df.append(filterData)

            filterData = df.loc[df['BagName'] == everyBag].reset_index(drop = True)
            if model == None:
                label.append(np.array(filterData.iloc[0]['BagLabels']).reshape(1,-1))
                filterData = filterData[col]
                instances.append(np.array(filterData).reshape(1,-1))
            else:# model == 'SVM':
                X, y = self.CreateStackedDataSetsMIL(filterData)
                label.append(y)
                filterData = filterData[col]
                instances.append(X)
            instances_df.append(filterData)

        
        if model == None:
            instances = np.concatenate(instances, axis = 0)
        return bags, instances, label, instances_df
    

    def CreateStackedDataSetsMIL(self, df):
        df = df.reset_index(drop=True)
        u = list(df['BagLabels'].unique())

        ListX = list()
        ListY = list()

        for m in u:
            _df = df[df['BagLabels'] == m]
            y = _df['BagLabels'].unique()
            y = list(y)[:1]
            X = _df.drop(['BagLabels', 'BagName', 'FileName'], axis = 1)
            ListX.append(np.array(X))
            ListY.append(np.array(y))

        X = np.concatenate(ListX, axis=0)
        y = np.concatenate(ListY, axis=0)

        X = np.vstack(X)
        y = np.hstack(y)


        return X, y

    def CreateStackedDataSetsSIL(self, df):
        df = df.reset_index(drop=True)
        u = list(df['BagLabels'].unique())

        ListX = list()
        ListY = list()

        for m in u:
            _df = df[df['BagLabels'] == m]
            y = _df['BagLabels']
            y = y.astype('int')
            X = _df.drop(['BagLabels', 'BagName', 'FileName'], axis = 1)
            ListX.append(np.array(X))
            ListY.append(np.array(y))

        X = np.concatenate(ListX, axis=0)
        y = np.concatenate(ListY, axis=0)

        X = np.vstack(X)
        y = np.hstack(y)

        return X, y
    
    def BagLabelV2(self, _df, _X, fileNames, _window, _isBinaryClassification = 1):
        window = 50*60*_window
        allDfs = list()
        bagCounter = 0

        for index in range(0, len(_df)):
            df = _df[index]
            df = df.dropna()
            X = _X[index]
            if(len(df) > 0):
                #print(fileNames[index])

                df['datetime'] = pd.to_datetime(df['datetime'])
                Ndf = pd.DataFrame(columns = df.columns)
                Ndf['BagLabels'] = None
                Ndf['BagName'] = None
                Ndf['FileName'] = None

                #None
                i = 0
                t0 = time.time()
                while i < len(X):
                    if i + window > (len(X)):
                        break
                    else:
                        minDatetime = np.min(X.values[i:i+window, (list(X.columns).index('datetime'))])
                        maxDatetime = np.max(X.values[i:i+window, (list(X.columns).index('datetime'))])
                        labels = list(X.values[i:i+window, (list(X.columns).index('y'))])
                        labels = set(labels)
                        if len(labels) == 1 and ('None' in labels or 'NV' in labels):

                            if _isBinaryClassification == 1:
                                _label = 0
                            else:
                                _label = labels

                            indexes = (df.loc[(df['datetime'] >= minDatetime) & (df['datetime'] < maxDatetime)]).index
                            tmp = pd.DataFrame(columns = Ndf.columns)
                            bagCounter = bagCounter + 1
                            tmp = (df.loc[(df['datetime'] >= minDatetime) & (df['datetime'] < maxDatetime)]).reset_index(drop=True)
                            tmp['BagName'] =  'Bag' + str(bagCounter)
                            tmp['BagLabels'] = _label
                            tmp['FileName'] = fileNames[index]
                            Ndf = Ndf.append(tmp)

                        i = i + window        
                t1 = time.time()            
                #print('Nones : ' + str(t1-t0))

                X['datetime'] = X['datetime'].values.astype('<M8[m]')

                t0 = time.time()
                AnnotationDict = {}
                maxTime = np.max(X['datetime']).to_pydatetime()
                minTime = np.min(X['datetime']).to_pydatetime()

                AX = X.copy()
                AX = AX[AX['y'] != 'None']

                AgitatedDict = (AX.groupby(['y'])['datetime'].apply(list)).to_dict()
                for d in AgitatedDict:
                    AgitatedDict[d] = list(set(AgitatedDict[d]))            

                t1 = time.time()
                #print('Dictionary : ' + str(t1-t0))

                maxIndex = X.datetime.idxmax()
                minIndex = X.datetime.idxmin()
                #Agitated

                #print(maxIndex)
                #print(minIndex)

                t0 = time.time()
                while len(AgitatedDict) > 0:

                    randomAnnotation = random.choice(list(AgitatedDict.keys()))
                    randomTimeStamp = random.choice(AgitatedDict[randomAnnotation])
                    LowerindexRandomTimeStamp = np.min(X[(X['datetime'] == randomTimeStamp)].index)

                    #print(maxIndex)
                    #print(minIndex)
                    #print(randomTimeStamp)
                    #print(LowerindexRandomTimeStamp)

                    LowerBound = (int(LowerindexRandomTimeStamp - (window/2)))
                    UpperBound = (int(LowerindexRandomTimeStamp + ((window/2))))

                    #print(UpperBound)
                    #print(LowerBound)


                    if UpperBound > maxIndex:
                        delta = UpperBound - maxIndex
                        UpperBound = maxIndex
                        LowerBound = LowerBound - delta

                    if LowerBound < minIndex:
                        delta = minIndex - LowerBound
                        LowerBound = minIndex
                        if (UpperBound + delta > maxIndex):
                            UpperBound = maxIndex
                        else:
                            UpperBound = UpperBound + delta

                    #print(UpperBound)
                    #print(LowerBound)
                    #print('\n')

                    minDatetime = X.at[LowerBound, 'datetime']
                    maxDatetime = X.at[UpperBound, 'datetime']

                    if _isBinaryClassification == 1:
                        _label = 1
                    else:
                        _label = randomAnnotation


                    updateList = list()
                    myList = AgitatedDict[randomAnnotation]

                    for i in myList:
                        if(i>=minDatetime and i <=maxDatetime):
                            pass
                        else:
                            updateList.append(i)
                    AgitatedDict[randomAnnotation] =  updateList

                    if len(AgitatedDict[randomAnnotation]) == 0: 
                        AgitatedDict.pop(randomAnnotation, None)

                    tmp = pd.DataFrame(columns = Ndf.columns)
                    tmp = (df.loc[(df['datetime'] >= minDatetime) & (df['datetime'] <= maxDatetime)]).reset_index(drop=True)
                    bagCounter = bagCounter + 1                
                    tmp['BagName'] =  'Bag' + str(bagCounter)
                    tmp['BagLabels'] = _label
                    tmp['FileName'] = fileNames[index]

                    Ndf = Ndf.append(tmp)
                else:
                    pass
            else:
                break

            t1 = time.time()
            #print('Agitations : ' + str(t1-t0))
            Ndf['datetime'] = pd.to_datetime(Ndf['datetime'])
            Ndf['datetime'] = (Ndf['datetime'] - dt.datetime(1970,1,1)).dt.total_seconds()
            Ndf = Ndf.dropna() #Drop all None
            allDfs.append(Ndf)

        if len(allDfs) > 0:
            cDf = pd.concat(allDfs)
            return cDf
        else:
            return None


