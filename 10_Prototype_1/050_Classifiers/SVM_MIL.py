from libraries import *

def addDelta(p_0):
    p_0['unix'] = pd.to_datetime(p_0['datetime'], format='%Y-%m-%d %H:%M:%S')
    p_0['unix'] = (p_0['unix'] - dt.datetime(1970,1,1)).dt.total_seconds()
    deltaT = np.array([(p_0.datetime[i + 1] - p_0.datetime[i]) for i in range(len(p_0)-1)])
    deltaT = np.concatenate((np.array([0]), deltaT))
    p_0.insert(1, 'deltaT', deltaT)
    #p_0['Time'] = minTime + dt.timedelta(seconds= p_0['CumTime'])
    return p_0

def SVM_MIL(df, split, _isBinaryClassification):
    
    _BagOfInstances = BoI.BoI()
    
    colToRemove = list()
    for i in range(1,df.shape[1]):
        v = set(list(df.iloc[:, i]))
        if 0.0 in v and 0 in v and len(v) == 1:
            colToRemove.append((df.columns)[i])

    colToRemove.extend(['x1_mrc_0_1_to_0_5', 'x2_mrc_0_1_to_0_5', 'x3_mrc_0_1_to_0_5','x1_mrc_0_5_to_3', 'x2_mrc_0_5_to_3', 'x3_mrc_0_5_to_3','x1_mrc_3_and_more', 'x2_mrc_3_and_more', 'x3_mrc_3_and_more'])
    
    uniqueBags = df['BagName'].unique()
    trainset, testset = train_test_split(uniqueBags, test_size=split) 

    trainset = list(set(trainset.tolist()))
    trainset = df.loc[df['BagName'].isin(list(trainset))].reset_index(drop=True) 

    testset = list(set(testset.tolist()))
    testset = df.loc[df['BagName'].isin(list(testset))].reset_index(drop=True)
    
    Trainbags, X_train, y_train, Train_df = _BagOfInstances.CreateBagsOfInstances(trainset, colToRemove, 'SVM')
    Testbags, X_test, y_test, Test_df = _BagOfInstances.CreateBagsOfInstances(testset, colToRemove, 'SVM')
    
    trainer = Trainer()
    model = SVC()
    pipeline = [('scale', StandarizerBagsList()), ('disc_mapping', MILESMapping())]
    
    if _isBinaryClassification == 1:
        trainer.prepare(model, preprocess_pipeline=pipeline ,metrics=['acc', AUC, Specificity,TrueNegatives, TruePositives, FalseNegatives, FalsePositives, Precision, Recall])
    else: 
        trainer.prepare(model, preprocess_pipeline=pipeline)

    valid = KFold(n_splits=5, shuffle=True)
    
    history = trainer.fit(X_train, y_train, sample_weights='balanced', validation_strategy=valid, verbose=1)
    y_pred = trainer.predict(X_test)
    y_pred_train = trainer.predict(X_train)
    
    crTrain = classification_report(y_train, y_pred_train, output_dict = True)
    logging.info('Train Accuracy: ' + str(crTrain['accuracy']))
    cr = classification_report(y_test, y_pred, output_dict = True)
    logging.info('Test Accuracy: ' + str(cr['accuracy']))
    
    cr = classification_report(y_test, y_pred, output_dict = True)
    cm = np.array2string(confusion_matrix(y_test, y_pred))
    
    results = {}
    results['ClassificationReport'] = str(cr)
    results['ConfustionMatrix'] = str(cm)
    results['TrainResult'] = str(crTrain['accuracy'])
    results['TestResult'] = str(cr['accuracy'])
    if _isBinaryClassification == 1:
        results['AUC'] = Evaluation.AUC_ROC(y_test,y_pred)

    results['y_test'] = y_test
    results['y_pred'] = y_pred
        
    return results, trainer


def PrepareData(df, colToRemove = None, model = None):
    df = df.drop(colToRemove, axis = 'columns')
    bags = list(df['BagName'].unique())
    X = list()
    y = list()

    col = list(df.columns)
    if 'BagName' in col:
        col.remove('BagName')
    if 'BagLabels' in col:
        col.remove('BagLabels')        
    if 'y' in col:
        col.remove('y')        
    if 'FileName' in col:
        col.remove('FileName')                
    for everyBag in bags:

        filterData = df.loc[df['BagName'] == everyBag].reset_index(drop = True)
        y.append(filterData.iloc[0]['BagLabels'])
        filterData = filterData[col]
        if model == None:
            X.append(np.array(filterData).reshape(1,-1))
        else:
            X.append(np.array(filterData))

    if model == None:
        X = np.concatenate(X, axis = 0)
    return X, y

def Evaluate(dfs, Xs, FileNames, _evaal, windowOfAnnotations, clf, _isBinaryClassification):
    _BagOfInstances = BoI.BoI()
    _ReadDirectory = Directory.HandlingDirectories()
    fileNumber=  list()
    TestAccuracy = {}
    TrainAccuracy = {}
    Distribution = {} 
    
    for f in range(0, len(dfs)):
        df = dfs[f]
        _X = Xs[f]
        file = FileNames[f]
        try:
            
            BaggedData = _BagOfInstances.BagLabelV2([df], [_X], [file], windowOfAnnotations, _isBinaryClassification)
            colToRemove = list()
            for i in range(1,df.shape[1]):
                v = set(list(df.iloc[:, i]))
                if 0.0 in v and 0 in v and len(v) == 1:
                    colToRemove.append((df.columns)[i])
            
            colToRemove.extend(['x1_mrc_0_1_to_0_5', 'x2_mrc_0_1_to_0_5', 'x3_mrc_0_1_to_0_5','x1_mrc_0_5_to_3', 'x2_mrc_0_5_to_3', 'x3_mrc_0_5_to_3','x1_mrc_3_and_more', 'x2_mrc_3_and_more', 'x3_mrc_3_and_more'])
            
            if _isBinaryClassification == 0:
                BaggedData = _ReadDirectory.AnnotationsToInt(BaggedData)

            X, y = PrepareData(BaggedData, colToRemove, 'SVM')
            if len(set(list(y))) > 1:
                y_pred=clf.predict(X)
                Distribution[file] = str(dict(Counter(list(_X['y']))))
                cr = classification_report(y, y_pred, output_dict = True)
                TrainAccuracy[file] = cr['accuracy']
        
        except Exception as e:
            logging.info(str(file) + ' : ' + str(e))    
    
    
    Accuracy = list()
    for d in TrainAccuracy.keys():
        logging.info(d + ' : ' + str(TrainAccuracy[d]))
        _evaal.SaveResult(d + ' : ' +  str(TrainAccuracy[d]) + ' : ' + Distribution[d])
        Accuracy.append(TrainAccuracy[d])
    
    logging.info('Avg Accuracy (Training Files): ' + str(np.mean(Accuracy)))    
    _evaal.SaveResult('Avg Accuracy (Training Files): ' + str(np.mean(Accuracy)))
    _evaal.AppendResult(np.mean(Accuracy))
        

def Prepare(pathToBes, pathToFes, pwd, window, overlap, windowOfAnnotations, destination, _train_test_split, _isBinaryClassification):
    logging.info('Check for Multiple pids...')
    pwd = pwd.split(';')
    logging.info('Total pwds: %s', len(pwd))
    
    ReadDirectory = Directory.HandlingDirectories()
    BagOfInstances = BoI.BoI()

    for p_id in pwd:
        _evaal = Evaluation(p_id, destination + '060_Results/', 'SVM_MIL', window, overlap)
        _evaal.SaveResult('Date: ' + time.strftime("%Y%m%d"))
        _evaal.SaveResult('Subject: ' + p_id)
        _evaal.SaveResult('Window (in rows): ' +  str(window))
        _evaal.SaveResult('Overlap: ' + str(overlap))
        _evaal.SaveResult('Window of Annotations: ' + str(windowOfAnnotations))
        _evaal.SaveResult('Train-test split: ' + str(_train_test_split))

        logging.info('Classifer for ' + p_id)
        dfs, FileNames = ReadDirectory.ReadFiles(p_id, pathToFes, window, overlap)
        logging.info('Files Read: %s', str(len(dfs)))
        logging.info('Files FEs %s', ','.join(FileNames))
        
        Xs, FileNames = ReadDirectory.ReadFiles(p_id, pathToBes, window, overlap)
        logging.info('Files Read: %s', str(len(Xs)))
        logging.info('Files BFs %s', ','.join(FileNames))

        BaggedData = BagOfInstances.BagLabelV2(dfs, Xs, FileNames, windowOfAnnotations, _isBinaryClassification)
        if _isBinaryClassification == 0:
            BaggedData = ReadDirectory.AnnotationsToInt(BaggedData)
        try:
            results, clf = SVM_MIL(BaggedData, _train_test_split, _isBinaryClassification)
            fName = ('CM' + '_SVM_MIL' + str(p_id) + '_' + str(window) + '_' + str(overlap) + '_' + str(windowOfAnnotations))
            _evaal.SaveConfusionMatrix(p_id, 'SVM_MIL', fName , results['y_test'], results['y_pred'], destination + '060_Results/' + p_id + '/Images/ConfustionMatrix/')
            if _isBinaryClassification == 1:
                fName = ('AUC_ROC' + '_SVM_MIL' + str(p_id) + '_' + str(window) + '_' + str(overlap) + '_' + str(windowOfAnnotations))
                _evaal.SaveAUCROC(p_id, 'SVM_MIL', fName, results['y_test'], results['y_pred'], destination + '060_Results/' + p_id + '/Images/AUC_ROC/')
            for k in results:
                _evaal.SaveResult('\n\n' + str(k) + '\n' + str(results[k]))

            _evaal.AppendResult(p_id)
            _evaal.AppendResult('SVM_MIL')
            _evaal.AppendResult(results['TrainResult'])
            _evaal.AppendResult(results['TestResult'])

            #Evaluate(dfs, Xs, FileNames, _evaal, windowOfAnnotations, clf, _isBinaryClassification)
            _evaal.AppendResult('Evaluate not working')
            _evaal.AppendResult(_train_test_split)
            _evaal.AppendResult(window)
            _evaal.AppendResult(windowOfAnnotations)
            _evaal.CombinedResultAsDf()
        except Exception as e:
            print(e)
            _evaal.ResetResultForDf(p_id, 'SVM_MIL')
            _evaal.CombinedResultAsDf()
        
if __name__ == "__main__": 
    parser = argparse.ArgumentParser()
    parser.add_argument("source_fes", help="Path to read files.",type=str)
    parser.add_argument("source_bfs", help="Path to read files.",type=str)
    parser.add_argument("destination", help="Path to save files.",type=str)
    parser.add_argument("pwdid", help="Person with dementia id",type=str)
    parser.add_argument("window", help="Feature extraction window, in seconds.",type=int, default=60)
    parser.add_argument("overlap", help="Overlap of windows, in percentage (no percentage sign).",type=int,default=50)
    parser.add_argument("window_annotations", help="Annotations window, in minutes.",type=int,default=20)
    parser.add_argument("_train_test_split", help="train_test_split, in percentage (no percentage sign).",type=int,default=30)
    parser.add_argument("isBinaryClassification", help="isBinaryClassification, 1 = True, 0 = False.",type=int,default=1)


    args = parser.parse_args()
    
    source_fes = args.source_fes 
    source_bfs = args.source_bfs 
    destination = args.destination
    p_id = args.pwdid
    window = args.window
    overlap = args.overlap
    aWindow = args.window_annotations
    _train_test_split = args._train_test_split
    _isBinaryClassification = args.isBinaryClassification
    
    ReadDirectory = Directory.HandlingDirectories()
    ReadDirectory.CreatePathIfNotExists(destination + '/090_Logging')

                    
    logging.basicConfig(filename= destination + '/090_Logging' + '/SVM_MIL_'+str(window)+'_'+str(overlap)+'_'+str(aWindow)+'.log', level=logging.INFO, filemode="w")
    logging.info('Argument read...:')
    logging.info('Source FEs: %s', str(source_fes))
    logging.info('Source BEs: %s', str(source_bfs))
    logging.info('Destination: %s', str(destination))
    logging.info('Subject: %s', str(p_id))
    logging.info('Window (in rows): %s', str(window))
    logging.info('Overlap: %s', str(overlap) + '%')
    logging.info('Window Annotations: %s', str(aWindow) + '%')
    logging.info('Train-test split: %s', str(_train_test_split) + '%')
    logging.info('isBinaryClassification: %s', str(_isBinaryClassification))
    
    Prepare(source_bfs, source_fes, p_id, int(window)*50, overlap, aWindow, destination, float(_train_test_split/100), _isBinaryClassification)