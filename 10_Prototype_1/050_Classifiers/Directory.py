from libraries import *

class HandlingDirectories():
    
    def CreatePathIfNotExists(self, path):
        if not os.path.exists(path):
            os.makedirs(path)
            return path
        else:
            return path
        
    def AnnotationsToInt(self, df):
        DzneToInt = {}
        u = df['BagLabels'].unique()
        uniqueId = 1
        for everyDzne in u:
            if everyDzne == 'None':
                DzneToInt[everyDzne] = 0
            elif everyDzne == 'A':
                DzneToInt[everyDzne] = 1
            elif everyDzne == 'M':
                DzneToInt[everyDzne] = 2
            elif everyDzne == 'P':
                DzneToInt[everyDzne] = 3
            elif everyDzne == 'G':
                DzneToInt[everyDzne] = 4
            elif everyDzne == 'Ag':
                DzneToInt[everyDzne] = 5
            elif everyDzne == 'O':
                DzneToInt[everyDzne] = 6
            else:
                DzneToInt[everyDzne] = -1
                #uniqueId = uniqueId + 1
        df = df.replace({"BagLabels" : DzneToInt})
        #print(DzneToInt)
        return df
    
    def addDelta(self, p_0):
        p_0['unix'] = pd.to_datetime(p_0['datetime'], format='%Y-%m-%d %H:%M:%S')
        p_0['unix'] = (p_0['unix'] - dt.datetime(1970,1,1)).dt.total_seconds()
        deltaT = np.array([(p_0.datetime[i + 1] - p_0.datetime[i]) for i in range(len(p_0)-1)])
        deltaT = np.concatenate((np.array([0]), deltaT))
        p_0.insert(1, 'deltaT', deltaT)
        #p_0['Time'] = minTime + dt.timedelta(seconds= p_0['CumTime'])
        return p_0
    
    def GetFilesToRead(self, dfs):
        myDfs = list()
        for eF in list(dfs['FileName'].unique()):
            fDfs = dfs[dfs['FileName'] == eF]
            fDfs = self.addDelta(fDfs)
            myDfs.append(fDfs)

        cDf = pd.concat(myDfs) 

        _cDfs = cDf[['datetime', 'deltaT', 'BagLabels', 'FileName']]
        TotalSecondsForEachAnnotations = _cDfs.groupby(['FileName','BagLabels']).agg({'deltaT': 'sum'}).reset_index()
        TotalSecondsForAggAnnotations = _cDfs.groupby(['FileName']).agg({'deltaT': 'sum'}).reset_index()
        _cDfs = pd.merge(TotalSecondsForEachAnnotations, TotalSecondsForAggAnnotations, left_on='FileName', right_on='FileName', how='inner')
        _cDfs['percentage'] = np.round( (_cDfs['deltaT_x'] / _cDfs['deltaT_y']) * 100,-1)
        _cDfs = _cDfs[['FileName', 'BagLabels', 'percentage']]
        #_cDfs.sort_values(by=['BagLabels', 'percentage'])


        bL = list(_cDfs['BagLabels'].unique())
        distribution = list(_cDfs['percentage'].unique())
        
        selectedFiles = list()

        for _bagLabel in bL:
            for _dis in distribution:
                fil = _cDfs[(_cDfs['BagLabels'] == _bagLabel) & (_cDfs['percentage'] == _dis)]
                if len(fil) > 0:
                    selectedFiles.append(fil.iloc[0]['FileName'])

        return list(set(selectedFiles))

    

    def ReadFiles(self, pid, pathToFiles, window, overlap):
        p_id = pid
        p = {}
        directories = os.listdir(pathToFiles)
        
        fdirectories = []
        for d in directories:
            if d == 'main.py' or d == 'run.sh' or d == '.ipynb_checkpoints':
                pass
            else:
                fdirectories.append(d)
        
        for everyDay in fdirectories:
            pwd = os.listdir(pathToFiles +everyDay)
            for everyPWD in pwd:
                if(everyPWD == p_id):
                    path = pathToFiles + everyDay + '/' + everyPWD
                    
                    allCSVs = os.listdir(path)
                    CSVName = str(window) + '_' + str(overlap) + '_' + everyPWD + '_' + everyDay + '.csv'
                    for everyCSV in allCSVs:
                        if(everyCSV == CSVName):
                            path = path+ '/' + everyCSV
                            try:
                                file = pd.read_csv(path)
                                if len(file) > 0:
                                    p[everyPWD + '_' + everyDay] = file
                                else:
                                    raise Exception('File Read but is empty.' + str(CSVName))
                            except Exception as e:
                                print(e)
                                print('This file was not read: ' + path + '\nFile Size: ' + str(os.stat(path).st_size) + '\n')

        p = OrderedDict(sorted(p.items()))
        p_0 = list(p.values())
        p_fileName = list(p.keys())

        return p_0, p_fileName