from libraries import *
    
def RF_SIL(XDf, split, _isBinaryClassification):
    
    y = XDf['BagLabels']
    y = y.astype('int')
    X = XDf.drop(['BagLabels', 'BagName', 'FileName'], axis = 1)
    
    #df = X.merge(y.rename('BagLabels'), left_index=True, right_index=True)
    #trainset, testset = train_test_split(df, test_size=split)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=split, stratify=y)
    
#     X_train = trainset.drop('BagLabels', axis = 1)
#     y_train = trainset['BagLabels']

#     X_test = testset.drop('BagLabels', axis = 1)
#     y_test = testset['BagLabels']
    
    clf = RandomForestClassifier(n_estimators=500, min_samples_split = 2, min_samples_leaf = 1, max_depth = 15)
    clf.fit(X_train,y_train)
    y_pred=clf.predict(X_test)
    
    cr = classification_report(y_test, y_pred)
    cm = np.array2string(confusion_matrix(y_test, y_pred))
    
    logging.info(("Accuracy: - Train",np.mean(cross_val_score(clf, X_train, y_train, cv=5)) ))
    logging.info(("Accuracy - Test:",metrics.accuracy_score(y_test, clf.predict(X_test))))
    
    results = {}
    results['ClassificationReport'] = str(cr)
    results['ConfustionMatrix'] = str(cm)
    results['TrainResult'] = np.mean(cross_val_score(clf, X_train, y_train, cv=5))
    results['TestResult'] = metrics.accuracy_score(y_test, clf.predict(X_test))
    
    if _isBinaryClassification == 1:
        results['AUC'] = Evaluation.AUC_ROC(y_test,y_pred)
    results['y_test'] = y_test
    results['y_pred'] = y_pred
    
    return results, clf

def PrepareData(baggedDf):
    y = baggedDf['BagLabels']
    y = y.astype('int')
    X = baggedDf.drop(['BagLabels', 'BagName', 'FileName'], axis = 1)
        
    return X, y

def Evaluate(dfs, Xs, FileNames, _evaal, windowOfAnnotations, clf, _isBinaryClassification):
    fileNumber=  list()
    TestAccuracy = {}
    TrainAccuracy = {}
    Distribution = {} 
    
    _ReadDirectory = Directory.HandlingDirectories()
    _BagOfInstances = BoI.BoI()
    
    for f in range(0, len(dfs)):
        df = dfs[f]
        X = Xs[f].reset_index(drop=True)
        file = FileNames[f]
        try:
            
            BaggedData = _BagOfInstances.BagLabel([df], [X], [file], windowOfAnnotations,_isBinaryClassification)
            Distribution[file] = str(dict(Counter(list(X['y']))))
            if _isBinaryClassification == 0:
                BaggedData = _ReadDirectory.AnnotationsToInt(BaggedData)
            X, y = PrepareData(BaggedData)
            y_pred=clf.predict(X)
            cr = classification_report(y, y_pred, output_dict = True)
            TrainAccuracy[file] = cr['accuracy']
                
        except Exception as e:
            logging.info(str(file) + ' : ' + str(e))
    
    Accuracy = list()
    for d in TrainAccuracy.keys():
        logging.info(d + ' : ' + str(TrainAccuracy[d])+ ' : ' + Distribution[d])
        _evaal.SaveResult(d + ' : ' +  str(TrainAccuracy[d]) + ' : ' + Distribution[d])
        Accuracy.append(TrainAccuracy[d])
    
    logging.info('Avg Accuracy (Training Files): ' + str(np.mean(Accuracy)))
    _evaal.SaveResult('Avg Accuracy (Training Files): ' + str(np.mean(Accuracy)))
    _evaal.AppendResult(np.mean(Accuracy))

    
        
def Prepare(pathToBes, pathToFes, pwd, window, overlap, windowOfAnnotations, destination, _train_test_split, _isBinaryClassification):
    logging.info('Check for Multiple pids...')
    pwd = pwd.split(';')
    logging.info('Total pwds: %s', len(pwd))
    
    ReadDirectory = Directory.HandlingDirectories()
    BagOfInstances = BoI.BoI()

    for p_id in pwd:
        _evaal = Evaluation(p_id, destination + '060_Results/', 'RF_SIL', window, overlap)
        _evaal.SaveResult('Date: ' + time.strftime("%Y%m%d"))
        _evaal.SaveResult('Subject: ' + p_id)
        _evaal.SaveResult('Window (in rows): ' +  str(window))
        _evaal.SaveResult('Overlap: ' + str(overlap))
        _evaal.SaveResult('Window of Annotations: ' + str(windowOfAnnotations))
        _evaal.SaveResult('Train-test split: ' + str(_train_test_split))
        
        logging.info('Classifer for ' + p_id)

        ReadDirectory = Directory.HandlingDirectories()

        dfs, FileNames = ReadDirectory.ReadFiles(p_id, pathToFes, window, overlap)
        logging.info('Files Read: %s', str(len(dfs)))
        logging.info('Files FEs %s', ','.join(FileNames))
        
        Xs, FileNames = ReadDirectory.ReadFiles(p_id, pathToBes, window, overlap)
        logging.info('Files Read: %s', str(len(Xs)))
        logging.info('Files BFs %s', ','.join(FileNames))

        BaggedData = BagOfInstances.BagLabelV2(dfs, Xs, FileNames, windowOfAnnotations,_isBinaryClassification)
        if _isBinaryClassification == 0:
            BaggedData = ReadDirectory.AnnotationsToInt(BaggedData)
        try:
            results, clf = RF_SIL(BaggedData, _train_test_split, _isBinaryClassification)
            fName = ('CM' + '_RF_SIL' + str(p_id) + '_' + str(window) + '_' + str(overlap) + '_' + str(windowOfAnnotations))
            _evaal.SaveConfusionMatrix(p_id, 'RF_SIL', fName , results['y_test'], results['y_pred'], destination + '060_Results/' + p_id + '/Images/ConfustionMatrix/')
            if _isBinaryClassification == 1:
                fName = ('AUC_ROC' + '_RF_SIL' + str(p_id) + '_' + str(window) + '_' + str(overlap) + '_' + str(windowOfAnnotations))
                _evaal.SaveAUCROC(p_id, 'RF_SIL', fName, results['y_test'], results['y_pred'], destination + '060_Results/' + p_id + '/Images/AUC_ROC/')

            for k in results:
                _evaal.SaveResult('\n\n' + str(k) + '\n' + str(results[k]))

            _evaal.AppendResult(p_id)
            _evaal.AppendResult('RF_SIL')
            _evaal.AppendResult(results['TrainResult'])
            _evaal.AppendResult(results['TestResult'])

            #Evaluate(dfs, Xs, FileNames, _evaal, windowOfAnnotations, clf, _isBinaryClassification)
            _evaal.AppendResult('NA')
            _evaal.AppendResult(_train_test_split)
            _evaal.AppendResult(window)
            _evaal.AppendResult(windowOfAnnotations)
            _evaal.CombinedResultAsDf()
        except:
            _evaal.ResetResultForDf(p_id, 'RF_SIL')
            _evaal.CombinedResultAsDf()
                    

if __name__ == "__main__": 
    parser = argparse.ArgumentParser()
    parser.add_argument("source_fes", help="Path to read files.",type=str)
    parser.add_argument("source_bfs", help="Path to read files.",type=str)
    parser.add_argument("destination", help="Path to save files.",type=str)
    parser.add_argument("pwdid", help="Person with dementia id",type=str)
    parser.add_argument("window", help="Feature extraction window, in seconds.",type=int, default=60)
    parser.add_argument("overlap", help="Overlap of windows, in percentage (no percentage sign).",type=int,default=50)
    parser.add_argument("window_annotations", help="Annotations window, in minutes.",type=int,default=20)
    parser.add_argument("_train_test_split", help="train_test_split, in percentage (no percentage sign).",type=int,default=30)
    parser.add_argument("isBinaryClassification", help="isBinaryClassification, 1 = True, 0 = False.",type=int,default=1)

    args = parser.parse_args()
    
    source_fes = args.source_fes 
    source_bfs = args.source_bfs 
    destination = args.destination
    p_id = args.pwdid
    window = args.window
    overlap = args.overlap
    aWindow = args.window_annotations
    _train_test_split = args._train_test_split
    _isBinaryClassification = args.isBinaryClassification
    
    ReadDirectory = Directory.HandlingDirectories()
    ReadDirectory.CreatePathIfNotExists(destination + '/090_Logging')

                    
    logging.basicConfig(filename= destination + '/090_Logging' + '/RandomForest_SIL_'+str(window)+'_'+str(overlap)+'_'+str(aWindow)+'.log', level=logging.INFO, filemode="w")
    logging.info('Argument read...:')
    logging.info('Source FEs: %s', str(source_fes))
    logging.info('Source BEs: %s', str(source_bfs))
    logging.info('Destination: %s', str(destination))
    logging.info('Subject: %s', str(p_id))
    logging.info('Window (in rows): %s', str(window))
    logging.info('Overlap: %s', str(overlap) + '%')
    logging.info('Window Annotations: %s', str(aWindow) + '%')
    logging.info('Train-test split: %s', str(_train_test_split) + '%')
    logging.info('isBinaryClassification: %s', str(_isBinaryClassification))
    
    Prepare(source_bfs, source_fes, p_id, int(window)*50, overlap, aWindow, destination, float(_train_test_split/100), _isBinaryClassification)