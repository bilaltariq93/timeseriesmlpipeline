import numpy as np
import time
from sklearn.metrics import roc_curve,roc_auc_score
from sklearn.metrics import auc
import os
import pandas as pd
from sklearn.metrics import classification_report,confusion_matrix
import seaborn as sns
import matplotlib.pyplot as plt


class Evaluation():
    timestrTxt = ''
    dfPath = ''
    resultForDf = list()
    
    def __init__(self, pid, destination, model, window, overlap):
        self.timestrTxt = self.CreatePathIfNotExists(destination + '/' + pid ) + '/' + (time.strftime("%Y%m%d") + '_' + pid + '_' + model + '_' + str(window) + '_' + str(overlap) + '.txt')
        self.dfPath = destination + (time.strftime("%Y%m%d") + '_' + str(window) + '_' + str(overlap) + '_CompleteResults.csv')
        open(self.timestrTxt, 'w').close()
        
        self.resultForDf = list()
        if os.path.isfile(self.dfPath):
            pass
        else:
            df = pd.DataFrame(columns = ['Subject', 'Model', 'AccuracyTrain', 'AccuracyTest', 'AccuracyAvg', 'Train-Test-Split', 'Window', 'AnnotationWindow'])
            df.to_csv(self.dfPath, index=False)

    def CreatePathIfNotExists(self, path):
        if not os.path.exists(path):
            os.makedirs(path)
            return path
        else:
            return path
        
    def ResetResultForDf(self, p_id, model):
        self.resultForDf = list()
        self.AppendResult(p_id)
        self.AppendResult(model)
        self.AppendResult('Error')
        self.AppendResult('')
        self.AppendResult('')
        self.AppendResult('')
        self.AppendResult('')
        self.AppendResult('')
        
    def SaveConfusionMatrix(self, pid, model, fileName, y_test, y_pred, destination):
        self.CreatePathIfNotExists(destination)
        fig = plt.figure()
        fig = plt.figure(figsize=(8, 6))
        fig = plt.title('Confusion Matrix - ' + pid + '(' + model + ')', size=16)
        fig = sns.heatmap(confusion_matrix(y_test, y_pred), annot=True, cmap='Blues', fmt='g')
        fig = fig.get_figure()
        fig.savefig(destination + fileName+ '.png')

    def SaveAUCROC(self, pid, model, fileName, y_test, y_pred, destination):
        try:
            self.CreatePathIfNotExists(destination)
            fpr, tpr, threshold = roc_curve(np.array(y_test), y_pred)
            roc_auc = auc(fpr, tpr)
            plt.title('Receiver Operating Characteristic - ' + pid + '(' + model + ')')
            plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
            plt.legend(loc = 'lower right')
            plt.plot([0, 1], [0, 1],'r--')
            plt.xlim([0, 1])
            plt.ylim([0, 1])
            plt.ylabel('True Positive Rate')
            plt.xlabel('False Positive Rate')
            plt.savefig(destination + fileName+  '.png')
        except Exception as e:
            print(e)
        
    def AUC_ROC(y_test, y_pred):
        fpr, tpr, threshold = roc_curve(np.array(y_test), y_pred)
        roc_auc = auc(fpr, tpr)
        return roc_auc

    def SaveResult(self, txt):
        result = open(self.timestrTxt,'a')
        result.write(txt + '\n')
        result.close()
    
    def AppendResult(self, txt):
        self.resultForDf.append(txt)
    
    def CombinedResultAsDf(self):
        df = pd.read_csv(self.dfPath)
        df.loc[len(df)] = self.resultForDf
        df.to_csv(self.dfPath, index=False)

        
        
        