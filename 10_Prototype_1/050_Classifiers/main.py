from libraries import *

def APR_MIL(df, split):
    results = {}
    _BagOfInstances = BoI.BoI()
    
    colToRemove = list()
    for i in range(1,df.shape[1]):
        v = set(list(df.iloc[:, i]))
        if 0.0 in v and 0 in v and len(v) == 1:
            colToRemove.append((df.columns)[i])
    colToRemove.extend(['x1_mrc_0_1_to_0_5', 'x2_mrc_0_1_to_0_5', 'x3_mrc_0_1_to_0_5','x1_mrc_0_5_to_3', 'x2_mrc_0_5_to_3', 'x3_mrc_0_5_to_3','x1_mrc_3_and_more', 'x2_mrc_3_and_more', 'x3_mrc_3_and_more'])
    
    bags, X, y, dfNot = _BagOfInstances.CreateBagsOfInstances(df, colToRemove, 'APR')
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=split, stratify=y)
    
    metrics = ['acc', AUC, Specificity,TrueNegatives, TruePositives, FalseNegatives, FalsePositives, Precision, Recall]

    trainer = Trainer()
    model = APR(thres=0.5, epsilon=0.1, step=5, verbose=1)
    history = model.fit(X_train, y_train)
    y_pred = model.predict(X_test)
    
    y_pred_train = model.predict(X_train)
    crTrain = classification_report(y_train,y_pred_train, output_dict=True)
    logging.info('Train Accuracy: ' + str(crTrain['accuracy']))
    
    cr = classification_report(y_test,y_pred, output_dict=True)
    logging.info('Test Accuracy: ' + str(cr['accuracy']))
    cm = np.array2string(confusion_matrix(y_test,y_pred))
    
    results['ClassificationReport'] = str(cr)
    results['ConfustionMatrix'] = str(cm)
    results['TrainResult'] = crTrain['accuracy']
    results['TestResult'] = cr['accuracy']
    results['AUC'] = Evaluation.AUC_ROC(y_test,y_pred)
    results['y_test'] = y_test
    results['y_pred'] = y_pred
    
    
    return results, model

def AdaBoosting_SIL(df, split, _isBinaryClassification):
    y = df['BagLabels']
    y = y.astype('int')
    X = df.drop(['BagLabels', 'BagName', 'FileName'], axis = 1)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=split, stratify=y)
    
    clf = AdaBoostClassifier(n_estimators=100)
    clf.fit(X_train,y_train)
    y_pred=clf.predict(X_test)
    cr  = ("Accuracy:",metrics.accuracy_score(y_test, y_pred))
    
    logging.info(("Accuracy: - Train",np.mean(cross_val_score(clf, X_train, y_train, cv=5)) ))
    logging.info(("Accuracy - Test:",metrics.accuracy_score(y_test, clf.predict(X_test))))
    
    cm = confusion_matrix(y_test, y_pred)
    results = {}
    results['ClassificationReport'] = str(cr)
    results['ConfustionMatrix'] = str(cm)
    results['TrainResult'] = np.mean(cross_val_score(clf, X_train, y_train, cv=5))
    results['TestResult'] = metrics.accuracy_score(y_test, clf.predict(X_test))
    
    if _isBinaryClassification == 1:
        results['AUC'] = Evaluation.AUC_ROC(y_test,y_pred)
    results['y_test'] = y_test
    results['y_pred'] = y_pred

    return results, clf

def AdaBoosting_MIL(df, split, _isBinaryClassification):
    
    _BagOfInstances = BoI.BoI()
    bags, X, y, dfnOT = _BagOfInstances.CreateBagsOfInstances(df)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=split, stratify=y)
    clf = AdaBoostClassifier(learning_rate= 0.01, n_estimators= 500)
    clf.fit(X_train,y_train)
    y_pred=clf.predict(X_test)
    cr  = ("Accuracy:",metrics.accuracy_score(y_test, y_pred))
    cm = confusion_matrix(y_test, y_pred)
    
    logging.info(("Accuracy: - Train",np.mean(cross_val_score(clf, X_train, y_train, cv=5)) ))
    logging.info(("Accuracy - Test:",metrics.accuracy_score(y_test, clf.predict(X_test))))
    
    results = {}
    results['ClassificationReport'] = str(cr)
    results['ConfustionMatrix'] = str(cm)
    results['TrainResult'] = np.mean(cross_val_score(clf, X_train, y_train, cv=5))
    results['TestResult'] = metrics.accuracy_score(y_test, clf.predict(X_test))
    
    if _isBinaryClassification == 1:
        results['AUC'] = Evaluation.AUC_ROC(y_test,y_pred)
    results['y_test'] = y_test
    results['y_pred'] = y_pred

    return results, clf

def RF_SIL(XDf, split, _isBinaryClassification):
    
    y = XDf['BagLabels']
    y = y.astype('int')
    X = XDf.drop(['BagLabels', 'BagName', 'FileName'], axis = 1)
    
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=split, stratify=y)
    
    clf = RandomForestClassifier(n_estimators=500, min_samples_split = 2, min_samples_leaf = 1, max_depth = 15)
    clf.fit(X_train,y_train)
    y_pred=clf.predict(X_test)
    
    cr = classification_report(y_test, y_pred)
    cm = np.array2string(confusion_matrix(y_test, y_pred))
    
    logging.info(("Accuracy: - Train",np.mean(cross_val_score(clf, X_train, y_train, cv=5)) ))
    logging.info(("Accuracy - Test:",metrics.accuracy_score(y_test, clf.predict(X_test))))
    
    results = {}
    results['ClassificationReport'] = str(cr)
    results['ConfustionMatrix'] = str(cm)
    results['TrainResult'] = np.mean(cross_val_score(clf, X_train, y_train, cv=5))
    results['TestResult'] = metrics.accuracy_score(y_test, clf.predict(X_test))
    
    if _isBinaryClassification == 1:
        results['AUC'] = Evaluation.AUC_ROC(y_test,y_pred)
    results['y_test'] = y_test
    results['y_pred'] = y_pred
    
    return results, clf

def SVM_MIL(df, split, _isBinaryClassification):
    _BagOfInstances = BoI.BoI()
    
    colToRemove = list()
    for i in range(1,df.shape[1]):
        v = set(list(df.iloc[:, i]))
        if 0.0 in v and 0 in v and len(v) == 1:
                if ((df.columns)[i] != 'BagLabels'):
                    colToRemove.append((df.columns)[i])

    colToRemove.extend(['x1_mrc_0_1_to_0_5', 'x2_mrc_0_1_to_0_5', 'x3_mrc_0_1_to_0_5','x1_mrc_0_5_to_3', 'x2_mrc_0_5_to_3', 'x3_mrc_0_5_to_3','x1_mrc_3_and_more', 'x2_mrc_3_and_more', 'x3_mrc_3_and_more'])

    bags, X, y, dfNot = _BagOfInstances.CreateBagsOfInstances(df, colToRemove, 'SVM')
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=split, stratify=y)

    trainer = Trainer()
    model = SVC()
    pipeline = [('scale', StandarizerBagsList()), ('disc_mapping', MILESMapping())]
    
    if _isBinaryClassification == 1:
        trainer.prepare(model, preprocess_pipeline=pipeline ,metrics=['acc', AUC, Specificity,TrueNegatives, TruePositives, FalseNegatives, FalsePositives, Precision, Recall])
    else: 
        trainer.prepare(model, preprocess_pipeline=pipeline)

    valid = KFold(n_splits=5, shuffle=True)
    
    history = trainer.fit(X_train, y_train, sample_weights='balanced', validation_strategy=valid, verbose=1)
    y_pred = trainer.predict(X_test)
    y_pred_train = trainer.predict(X_train)
    
    crTrain = classification_report(y_train, y_pred_train, output_dict = True)
    logging.info('Train Accuracy: ' + str(crTrain['accuracy']))
    cr = classification_report(y_test, y_pred, output_dict = True)
    logging.info('Test Accuracy: ' + str(cr['accuracy']))
    
    cr = classification_report(y_test, y_pred, output_dict = True)
    cm = np.array2string(confusion_matrix(y_test, y_pred))
    
    results = {}
    results['ClassificationReport'] = str(cr)
    results['ConfustionMatrix'] = str(cm)
    results['TrainResult'] = str(crTrain['accuracy'])
    results['TestResult'] = str(cr['accuracy'])
    if _isBinaryClassification == 1:
        results['AUC'] = Evaluation.AUC_ROC(y_test,y_pred)

    results['y_test'] = y_test
    results['y_pred'] = y_pred
        
    return results, trainer


def SVM_SIL(df, split, _isBinaryClassification):
    _BagOfInstances = BoI.BoI()
    
    trainset, testset = train_test_split(df, test_size= split)
    
    if _isBinaryClassification == 0:
        X_train, y_train = _BagOfInstances.CreateStackedDataSetsSIL(trainset)
        X_test, y_test = _BagOfInstances.CreateStackedDataSetsSIL(testset)
    else:
        y_train = np.array(trainset['BagLabels'].astype(int))
        X_train = np.array(trainset.drop(['BagLabels','BagName', 'FileName'], axis = 1))
        y_test = list(testset['BagLabels'].astype(int))
        X_test = np.array(testset.drop(['BagLabels','BagName', 'FileName'], axis = 1))
        
    clf = SVC(C = 1.0, gamma=  0.0001, kernel=  'rbf', probability=False, class_weight='balanced')
    valid = KFold(n_splits=5, shuffle=True)
    clf.fit(X_train, y_train)# Make prediction
    y_pred = clf.predict(X_test)# Evaluate our model
    
    logging.info(("Accuracy: - Train",np.mean(cross_val_score(clf, X_train, y_train, cv=5)) ))
    logging.info(("Accuracy - Test:",metrics.accuracy_score(y_test, clf.predict(X_test))))
    
    cr = (classification_report(y_test, y_pred))
    cm = (confusion_matrix(y_test, y_pred))
    
    results = {}
    results['ClassificationReport'] = str(cr)
    results['ConfustionMatrix'] = str(cm)
    results['TrainResult'] = np.mean(cross_val_score(clf, X_train, y_train, cv=5))
    results['TestResult'] = metrics.accuracy_score(y_test, clf.predict(X_test))
    results['y_test'] = y_test
    results['y_pred'] = y_pred
    
    if _isBinaryClassification == 1:
        results['AUC'] = Evaluation.AUC_ROC(y_test,y_pred)
    
    return results, clf

def Prepare(pathToBes, pathToFes, pwd, window, overlap, windowOfAnnotations, destination, _train_test_split, _isBinaryClassification):
    logging.info('Check for Multiple pids...')
    pwd = pwd.split(';')
    logging.info('Total pwds: %s', len(pwd))

    _BagOfInstances = BoI.BoI()
    _ReadDirectory = Directory.HandlingDirectories()

    for p_id in pwd:
        logging.info('Classifer for ' + p_id)

        dfs, FileNames = _ReadDirectory.ReadFiles(p_id, pathToFes, window, overlap)
        logging.info('Files Read: %s', str(len(dfs)))
        logging.info('Files FEs %s', ','.join(FileNames))

        Xs, FileNames = _ReadDirectory.ReadFiles(p_id, pathToBes, window, overlap)
        logging.info('Files Read: %s', str(len(Xs)))
        logging.info('Files BFs %s', ','.join(FileNames))

        BaggedData = _BagOfInstances.BagLabelV2(dfs, Xs, FileNames, windowOfAnnotations)
        
        if _isBinaryClassification == 1:
            Classifiers = ['APR','BoostingSIL', 'SVMSIL', 'SVMMIL', 'RF']
        else:
            Classifiers = ['BoostingMIL', 'BoostingSIL', 'SVMSIL', 'SVMMIL', 'RF']
            BaggedData = ReadDirectory.AnnotationsToInt(BaggedData)
            
        
        for c in Classifiers:
            try:
                _evaal = Evaluation(p_id, destination + '060_Results/', c, window, overlap)
                _evaal.SaveResult('Date: ' + time.strftime("%Y%m%d"))
                _evaal.SaveResult('Subject: ' + p_id)
                _evaal.SaveResult('Window (in rows): ' +  str(window))
                _evaal.SaveResult('Overlap: ' + str(overlap))
                _evaal.SaveResult('Window of Annotations: ' + str(windowOfAnnotations))
                _evaal.SaveResult('Train-test split: ' + str(_train_test_split))
                
                if(c == 'APR'):
                    results, clf = APR_MIL(BaggedData, _train_test_split)
                elif(c == 'BoostingSIL'):
                    results, clf = AdaBoosting_SIL(BaggedData, _train_test_split, _isBinaryClassification)
                elif(c == 'BoostingMIL'):
                    results, clf = AdaBoosting_MIL(BaggedData, _train_test_split, _isBinaryClassification)             
                elif(c == 'SVMSIL'):
                    results, clf = SVM_SIL(BaggedData, _train_test_split, _isBinaryClassification)
                elif(c == 'SVMMIL'):
                    results, clf = SVM_MIL(BaggedData, _train_test_split, _isBinaryClassification)
                elif(c == 'RF'):
                    results, clf = RF_SIL(BaggedData, _train_test_split, _isBinaryClassification)

                fName = ('CM_' + c + str(p_id) + '_' + str(window) + '_' + str(overlap) + '_' + str(windowOfAnnotations))
                _evaal.SaveConfusionMatrix(p_id, c, fName , results['y_test'], results['y_pred'], destination + '060_Results/' + p_id + '/Images/ConfustionMatrix/')
                fName = ('AUC_ROC_' + c + str(p_id) + '_' + str(window) + '_' + str(overlap) + '_' + str(windowOfAnnotations))
                _evaal.SaveAUCROC(p_id, c , fName, results['y_test'], results['y_pred'], destination + '060_Results/' + p_id + '/Images/AUC_ROC/')

                for k in results:
                    _evaal.SaveResult('\n\n' + str(k) + '\n' + str(results[k]))

                _evaal.AppendResult(p_id)
                _evaal.AppendResult(c)
                _evaal.AppendResult(results['TrainResult'])
                _evaal.AppendResult(results['TestResult'])

                #Evaluate(dfs, Xs, FileNames, _evaal, windowOfAnnotations, clf)
                _evaal.AppendResult('N/A')
                _evaal.AppendResult(_train_test_split)
                _evaal.AppendResult(window)
                _evaal.AppendResult(windowOfAnnotations)
                _evaal.CombinedResultAsDf()
            except Exception as e:
                print(e)
                _evaal.ResetResultForDf(p_id, c)
                _evaal.CombinedResultAsDf()



if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument("source_fes", help="Path to read files.",type=str)
    parser.add_argument("source_bfs", help="Path to read files.",type=str)
    parser.add_argument("destination", help="Path to save files.",type=str)
    parser.add_argument("pwdid", help="Person with dementia id",type=str)
    parser.add_argument("window", help="Feature extraction window, in seconds.",type=int, default=60)
    parser.add_argument("overlap", help="Overlap of windows, in percentage (no percentage sign).",type=int,default=50)
    parser.add_argument("window_annotations", help="Annotations window, in minutes.",type=int,default=20)
    parser.add_argument("_train_test_split", help="train_test_split, in percentage (no percentage sign).",type=int,default=30)
    parser.add_argument("isBinaryClassification", help="isBinaryClassification, 1 = True, 0 = False.",type=int,default=1)
    
    args = parser.parse_args()
    
    source_fes = args.source_fes 
    source_bfs = args.source_bfs 
    destination = args.destination
    p_id = args.pwdid
    window = args.window
    overlap = args.overlap
    aWindow = args.window_annotations
    _train_test_split = args._train_test_split
    _isBinaryClassification = args.isBinaryClassification
    
    ReadDirectory = Directory.HandlingDirectories()
    ReadDirectory.CreatePathIfNotExists(destination + '/090_Logging')
    
    
    logging.basicConfig(filename= destination + '/090_Logging' + '/AllModels_'+str(window)+'_'+str(overlap)+'_'+str(aWindow)+'.log', level=logging.INFO, filemode="w")
    logging.info('Argument read...:')
    logging.info('Source FEs: %s', str(source_fes))
    logging.info('Source BEs: %s', str(source_bfs))
    logging.info('Destination: %s', str(destination))
    logging.info('Subject: %s', str(p_id))
    logging.info('Window (in rows): %s', str(window))
    logging.info('Overlap: %s', str(overlap) + '%')
    logging.info('Window Annotations: %s', str(aWindow) + '%')
    logging.info('Train-test split: %s', str(_train_test_split) + '%')
    logging.info('isBinaryClassification: %s', str(_isBinaryClassification))
    
    Prepare(source_bfs, source_fes, p_id, int(window)*50, overlap, aWindow, destination, float(_train_test_split/100), _isBinaryClassification)