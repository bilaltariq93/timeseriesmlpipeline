from libraries import *

class BoI():
    
    bC = 0
    def __init__(self):
        self.bC = 1
    
    def SetBagCounter(self, bags):
        self.bC = self.bC + bags
    
    def BagLabel(self, _df, _X, fileNames, _window, _isBinaryClassification = 1):
        bagCounter = self.bC
        allDfs = list()
        for index in range(0, len(_df)):
            df = pd.read_csv(_df[index])
            df = df.dropna()
            X = pd.read_csv(_X[index])

            if len(df) > 0:
                df['BagLabels'] = None
                df['BagName'] = None
                df['FileName'] = fileNames[index]
                #print(fileNames[index])

                window = 50*60*_window
                i = 0
                while i < len(X):
                    if i + window > (len(X)):
                        break

                    else:
                        minDatetime = np.min(X.values[i:i+window, (list(X.columns).index('datetime'))])
                        maxDatetime = np.max(X.values[i:i+window, (list(X.columns).index('datetime'))])


                        labels = list(X.values[i:i+window, (list(X.columns).index('y'))])
                        
                        if _isBinaryClassification == 1:
                            labels = set(labels)
                            if len(labels) == 1 and ('None' in labels or 'NV' in labels):
                                _label = 0
                            else:
                                _label = 1
                        elif _isBinaryClassification == 0:
                            _dis = dict(Counter(labels))
                            v, k = max((v, k) for k, v in _dis.items())
                            _label = k
                        else:
                            #regression
                            pass

                        indexes = (df.loc[(df['datetime'] >= minDatetime) & (df['datetime'] < maxDatetime)]).index
                        df.loc[min(indexes): max(indexes), 'BagName'] = 'Bag' + str(bagCounter)
                        df.loc[min(indexes): max(indexes), 'BagLabels'] = _label
                        #print(str(minDatetime) + ' : ' + str(maxDatetime) + ' : ' + str(_label)+ ' : ' + str(labels)+ ' : ' + str(min(indexes)) + ' : ' +  str(max(indexes)) )

                        i = (i + window)
                        bagCounter = bagCounter + 1

                df['datetime'] = pd.to_datetime(df['datetime'])
                df['datetime'] = (df['datetime'] - dt.datetime(1970,1,1)).dt.total_seconds()
                df = df.dropna() #Drop all None
                allDfs.append(df)

            else:
                pass
                #logging.info('File is null - Might contain nulls : ' + str(fileNames[index]))

        if len(allDfs) > 0:
            cDf = pd.concat(allDfs)
            return cDf
        else:
            return None

    def CreateBagsOfInstances(self, df, colToRemove = None, model = None):
        if colToRemove != None:
            df = df.drop(colToRemove, axis = 'columns')
        bags = list(df['BagName'].unique())
        instances = list()
        instances_df = list()
        label = list()

        col = list(df.columns)
        if 'BagName' in col:
            col.remove('BagName')
        if 'BagLabels' in col:
            col.remove('BagLabels')        
        if 'y' in col:
            col.remove('y')        
        if 'FileName' in col:
            col.remove('FileName')                
        for everyBag in bags:

#             filterData = df.loc[df['BagName'] == everyBag].reset_index(drop = True)
#             label.append(filterData.iloc[0]['BagLabels'])
#             filterData = filterData[col]
#             if model == None:
#                 instances.append(np.array(filterData).reshape(1,-1))
#             else:
#                 instances.append(np.array(filterData))
#             instances_df.append(filterData)

            filterData = df.loc[df['BagName'] == everyBag].reset_index(drop = True)
            if model == None:
                label.append(filterData.iloc[0]['BagLabels'])
                filterData = filterData[col]
                instances.append(np.array(filterData).reshape(1,-1))
            else:# model == 'SVM':
                X, y = self.CreateStackedDataSetsMIL(filterData)
                label.append(y)
                filterData = filterData[col]
                instances.append(X)
            instances_df.append(filterData)

        
        if model == None:
            instances = np.concatenate(instances, axis = 0)
        return bags, instances, label, instances_df
    

    def CreateStackedDataSetsMIL(self, df):
        df = df.reset_index(drop=True)
        u = list(df['BagLabels'].unique())
        
        ListX = list()
        ListY = list()

        for m in u:
            _df = df[df['BagLabels'] == m]
            y = _df['BagLabels'].unique()
            y = list(y)[:1]
            X = _df.drop(['BagLabels', 'BagName', 'FileName'], axis = 1)
            ListX.append(np.array(X))
            ListY.append(np.array(y))

        X = np.concatenate(ListX, axis=0)
        y = np.concatenate(ListY, axis=0)

        X = np.vstack(X)
        y = np.hstack(y)


        return X, y

    def CreateStackedDataSetsSIL(self, df):
        df = df.reset_index(drop=True)
        u = list(df['BagLabels'].unique())

        ListX = list()
        ListY = list()

        for m in u:
            _df = df[df['BagLabels'] == m]
            y = _df['BagLabels']
            y = y.astype('int')
            X = _df.drop(['BagLabels', 'BagName', 'FileName'], axis = 1)
            ListX.append(np.array(X))
            ListY.append(np.array(y))

        X = np.concatenate(ListX, axis=0)
        y = np.concatenate(ListY, axis=0)

        X = np.vstack(X)
        y = np.hstack(y)


        return X, y
