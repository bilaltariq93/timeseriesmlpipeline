from libraries import *

def EqualizeFeatureInstances(dfToEqualize):
    result = (dfToEqualize.groupby(['BagName']).size()).reset_index()
    result.columns = ['BagName', 'Count']
    uniqueInstances = list(result['Count'].unique())
    
    if len(uniqueInstances) == 1:
        return dfToEqualize
    else:
        minInstances = min(uniqueInstances)
        BagList = list((result['BagName'].loc[result['Count'] == minInstances]).unique())
        
        if max(uniqueInstances) - minInstances == 1:
            #Add rows
            for everyBag in BagList:
                m = (dfToEqualize.loc[dfToEqualize['BagName'] == everyBag]).mean()
                m.columns = dfToEqualize.columns
                m['BagName'] = everyBag
                
                dfToEqualize = dfToEqualize.append(m, ignore_index = True)
                dfToEqualize = dfToEqualize.reset_index(drop = True)
            
            return dfToEqualize
        else:
            #Remove them
            return dfToEqualize[~dfToEqualize['BagName'].isin(BagList)]


def PrepareTrainAndTestMIL(t, split = 0.3):
    _BagOfInstances = BoI.BoI()
    bags, X, y, df = _BagOfInstances.CreateBagsOfInstances(t)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=split, stratify=y)
#     uniqueBags = t['BagName'].unique()
#     trainset, testset = train_test_split(uniqueBags, test_size=split)
    
#     trainset = list(set(trainset.tolist()))
#     trainset = t.loc[t['BagName'].isin(list(trainset))].reset_index(drop=True) 
#     trainset = EqualizeFeatureInstances(trainset)
    
#     testset = list(set(testset.tolist()))
#     testset = t.loc[t['BagName'].isin(list(testset))].reset_index(drop=True)
#     testset = EqualizeFeatureInstances(testset)

#     Trainbags, TrainInstances, TrainLabel, df = _BagOfInstances.CreateBagsOfInstances(trainset)
#     Testbags, TestInstances, TestLabel, df = _BagOfInstances.CreateBagsOfInstances(testset)
    
    return X_train, y_train, X_test, y_test

def AdaBoosting_MIL(df, split, _isBinaryClassification):
    
    X_train, y_train, X_test, y_test = PrepareTrainAndTestMIL(df, split)
    clf = AdaBoostClassifier(learning_rate= 0.01, n_estimators= 500)
    clf.fit(X_train,y_train)
    y_pred=clf.predict(X_test)
    cr  = ("Accuracy:",metrics.accuracy_score(y_test, y_pred))
    cm = confusion_matrix(y_test, y_pred)
    
    logging.info(("Accuracy: - Train",np.mean(cross_val_score(clf, X_train, y_train, cv=5)) ))
    logging.info(("Accuracy - Test:",metrics.accuracy_score(y_test, clf.predict(X_test))))
    
    results = {}
    results['ClassificationReport'] = str(cr)
    results['ConfustionMatrix'] = str(cm)
    results['TrainResult'] = np.mean(cross_val_score(clf, X_train, y_train, cv=5))
    results['TestResult'] = metrics.accuracy_score(y_test, clf.predict(X_test))
    
    if _isBinaryClassification == 1:
        results['AUC'] = Evaluation.AUC_ROC(y_test,y_pred)
    results['y_test'] = y_test
    results['y_pred'] = y_pred

    return results, clf


def PrepareData(df, colToRemove = None, model = None):
    bags = list(df['BagName'].unique())

    X = list()
    y = list()

    col = list(df.columns)
    if 'BagName' in col:
        col.remove('BagName')
    if 'BagLabels' in col:
        col.remove('BagLabels')        
    if 'y' in col:
        col.remove('y')        
    if 'FileName' in col:
        col.remove('FileName')                
    for everyBag in bags:

        filterData = df.loc[df['BagName'] == everyBag].reset_index(drop = True)
        y.append(filterData.iloc[0]['BagLabels'])
        filterData = filterData[col]
        if model == None:
            X.append(np.array(filterData).reshape(1,-1))
        else:
            X.append(np.array(filterData))
        #instances_df.append(filterData)

    if model == None:
        X = np.concatenate(X, axis = 0)
    return X, y

def EvaluateRevised(BaggedData, clf, _evaal):
    results = {}
    _BagOfInstances = BoI.BoI()
    bags, X, y, instances_df = _BagOfInstances.CreateBagsOfInstances(BaggedData)
    
    #X, y = PrepareData(BaggedData)
    y_pred=clf.predict(X)
    cr = classification_report(y, y_pred, output_dict = True)
    cm = np.array2string(confusion_matrix(y,y_pred))
    _evaal.AppendResult(cr['accuracy'])
    
    results['ClassificationReport'] = str(cr)
    results['ConfustionMatrix'] = str(cm)
    results['TestResult'] = cr['accuracy']
    results['AUC'] = Evaluation.AUC_ROC(y,y_pred)
    results['y_test'] = y
    results['y_pred'] = y_pred
    return results
    
def Prepare(pathToBes, pathToFes, p_id_train, p_id_test, window, overlap, windowOfAnnotations, destination, _train_test_split, _isBinaryClassification):
    
    logging.info('Check for Multiple pids...')
    pwd = p_id_train.split(';')
    logging.info('Total pwds: %s', len(pwd))
    
    _ReadDirectory = Directory.HandlingDirectories()
    _BagOfInstances = BoI.BoI()
    
    ListOfBaggedData = list()
    
    for p_id in pwd:
        logging.info('Classifer for ' + p_id)

        dfs, FileNames = _ReadDirectory.ReadFiles(p_id, pathToFes, window, overlap)
        logging.info('Files Read: %s', str(len(dfs)))
        logging.info('Files FEs %s', ','.join(FileNames))
        
        Xs, FileNames = _ReadDirectory.ReadFiles(p_id, pathToBes, window, overlap)
        logging.info('Files Read: %s', str(len(Xs)))
        logging.info('Files BFs %s', ','.join(FileNames))

        BaggedData = _BagOfInstances.BagLabel(dfs, Xs, FileNames, windowOfAnnotations, _isBinaryClassification)
        _BagOfInstances.SetBagCounter(len(list(BaggedData['BagName'].unique())))
        ListOfBaggedData.append(BaggedData)
    
    BaggedData = pd.concat(ListOfBaggedData)
    
    _model = 'BoostingMIL'
    
    try:
        if _isBinaryClassification == 0:
            BaggedData = ReadDirectory.AnnotationsToInt(BaggedData)
            
        results, clf = AdaBoosting_MIL(BaggedData, _train_test_split, _isBinaryClassification)
        _evaal = Evaluation(p_id, destination + '061_Results/Training', _model, window, overlap)
        
        _evaal.SaveResult('Date: ' + time.strftime("%Y%m%d"))
        _evaal.SaveResult('Subject: ' + p_id)
        _evaal.SaveResult('Window (in rows): ' +  str(window))
        _evaal.SaveResult('Overlap: ' + str(overlap))
        _evaal.SaveResult('Window of Annotations: ' + str(windowOfAnnotations))
        _evaal.SaveResult('Train-test split: ' + str(_train_test_split))
        
        for k in results:
            _evaal.SaveResult('\n\n' + str(k) + '\n' + str(results[k]))
        
        fName = ('CM_' + _model + '_' + str(window) + '_' + str(overlap) + '_' + str(windowOfAnnotations))
        _evaal.SaveConfusionMatrix(p_id, _model, fName , results['y_test'], results['y_pred'], destination + '061_Results/Training/Images/')
        fName = ('AUC_' + _model +'_' + str(window) + '_' + str(overlap) + '_' + str(windowOfAnnotations))
        _evaal.SaveAUCROC(p_id, _model, fName, results['y_test'], results['y_pred'], destination + '061_Results/Training/Images/')

        
        p_id_test = p_id_test.split(';')
        for p_test in p_id_test:
            
            _evaal = Evaluation(p_test, destination + '061_Results', _model, window, overlap)
            _evaal.SaveResult('Date: ' + time.strftime("%Y%m%d"))
            _evaal.SaveResult('Subject (Train): ' + p_id)
            _evaal.SaveResult('Subject (Test): ' + p_test)
            _evaal.SaveResult('Window (in rows): ' +  str(window))
            _evaal.SaveResult('Overlap: ' + str(overlap))
            _evaal.SaveResult('Window of Annotations: ' + str(windowOfAnnotations))
            _evaal.SaveResult('Train-test split: ' + str(_train_test_split))

            _evaal.AppendResult(p_id_train)
            _evaal.AppendResult(p_test)
            _evaal.AppendResult(_model)
            _evaal.AppendResult(results['TrainResult']) #Training Result


            logging.info('Testing for ' + p_test)

            dfs, FileNames = _ReadDirectory.ReadFiles(p_test, pathToFes, window, overlap)
            logging.info('Files Read: %s', str(len(dfs)))
            logging.info('Files FEs %s', ','.join(FileNames))

            Xs, FileNames = _ReadDirectory.ReadFiles(p_test, pathToBes, window, overlap)
            logging.info('Files Read: %s', str(len(Xs)))
            logging.info('Files BFs %s', ','.join(FileNames))

            logging.info('Bagged Data for testing: \t' + p_test)
            BaggedData = _BagOfInstances.BagLabel(dfs, Xs, FileNames, windowOfAnnotations)
            logging.info('Bagged Data for testing: \t' + str(len(BaggedData)))
            
            results_test = EvaluateRevised(BaggedData, clf, _evaal)
            logging.info('Result Test done')
            for k in results_test:
                _evaal.SaveResult('\n\n' + str(k) + '\n' + str(results_test[k]))
            
            path = destination + '061_Results/' + p_test + '/Images/'
            fName = ('CM_' + _model + '_' + str(p_test) + '_' + str(window) + '_' + str(overlap) + '_' + str(windowOfAnnotations))
            _evaal.SaveConfusionMatrix(p_test, 'BoostingMIL', fName , results_test['y_test'], results_test['y_pred'], path)
            fName = ('AUC_ROC_'  + _model + '_' + str(p_test) + '_' + str(window) + '_' + str(overlap) + '_' + str(windowOfAnnotations))
            _evaal.SaveAUCROC(p_test, _model , fName, results_test['y_test'], results_test['y_pred'], path)
            
            _evaal.AppendResult(_train_test_split)
            _evaal.AppendResult(window)
            _evaal.AppendResult(windowOfAnnotations)
            _evaal.CombinedResultAsDf()
            
    except Exception as e:
        print('Error')
        print(e)

if __name__ == "__main__": 
    
    parser = argparse.ArgumentParser()
    parser.add_argument("source_fes", help="Path to read files.",type=str)
    parser.add_argument("source_bfs", help="Path to read files.",type=str)
    parser.add_argument("destination", help="Path to save files.",type=str)
    parser.add_argument("pwd_train", help="Person with dementia id for training",type=str)
    parser.add_argument("pwd_test", help="Person with dementia id for test",type=str)
    parser.add_argument("window", help="Feature extraction window, in seconds.",type=int, default=60)
    parser.add_argument("overlap", help="Overlap of windows, in percentage (no percentage sign).",type=int,default=50)
    parser.add_argument("window_annotations", help="Annotations window, in minutes.",type=int,default=20)
    parser.add_argument("_train_test_split", help="train_test_split, in percentage (no percentage sign).",type=int,default=30)
    parser.add_argument("isBinaryClassification", help="isBinaryClassification, 1 = True, 0 = False.",type=int,default=1)
    
    args = parser.parse_args()
    
    source_fes = args.source_fes 
    source_bfs = args.source_bfs 
    destination = args.destination
    p_id_train = args.pwd_train
    p_id_test = args.pwd_test
    window = args.window
    overlap = args.overlap
    aWindow = args.window_annotations
    _train_test_split = args._train_test_split
    _isBinaryClassification = args.isBinaryClassification
    
    ReadDirectory = Directory.HandlingDirectories()
    ReadDirectory.CreatePathIfNotExists(destination + '/091_Logging')
                    
    logging.basicConfig(filename= destination + '/091_Logging' + '/BoostingMIL_'+str(window)+'_'+str(overlap)+'_'+str(aWindow)+'.log', level=logging.INFO, filemode="w")
    logging.info('Argument read...:')
    logging.info('Source FEs: %s', str(source_fes))
    logging.info('Source BEs: %s', str(source_bfs))
    logging.info('Destination: %s', str(destination))
    logging.info('Training pids: %s', str(p_id_train))
    logging.info('Test pids: %s', str(p_id_test))
    logging.info('Window (in rows): %s', str(window))
    logging.info('Overlap: %s', str(overlap) + '%')
    logging.info('Window Annotations: %s', str(aWindow) + '%')
    logging.info('Train-test split: %s', str(_train_test_split) + '%')
    logging.info('isBinaryClassification: %s', str(_isBinaryClassification))
    
    Prepare(source_bfs, source_fes, p_id_train, p_id_test, int(window)*50, overlap, aWindow, destination, float(_train_test_split/100), _isBinaryClassification)