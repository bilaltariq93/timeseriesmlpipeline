from libraries import *

def APR_MIL(df, split):
    df = df.reset_index(drop=True)
    results = {}
    _BagOfInstances = BoI.BoI()
    
    colToRemove = list()
    for i in range(1,df.shape[1]):
        v = set(list(df.iloc[:, i]))
        if 0.0 in v and 0 in v and len(v) == 1:
            if((df.columns)[i] != 'BagLabels'):
                colToRemove.append((df.columns)[i])
    colToRemove.extend(['x1_mrc_0_1_to_0_5', 'x2_mrc_0_1_to_0_5', 'x3_mrc_0_1_to_0_5','x1_mrc_0_5_to_3', 'x2_mrc_0_5_to_3', 'x3_mrc_0_5_to_3','x1_mrc_3_and_more', 'x2_mrc_3_and_more', 'x3_mrc_3_and_more'])
    
#     trainset, testset = train_test_split(df, test_size = split)
#     Trainbags, X_train, y_train, df = _BagOfInstances.CreateBagsOfInstances(trainset, colToRemove, 'APR')
#     Testbags, X_test, y_test, df = _BagOfInstances.CreateBagsOfInstances(testset, colToRemove, 'APR')
    
    bags, X, y, _df = _BagOfInstances.CreateBagsOfInstances(df, colToRemove, 'APR')
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=split, stratify=y)
    
    metrics = ['acc', AUC, Specificity,TrueNegatives, TruePositives, FalseNegatives, FalsePositives, Precision, Recall]

    trainer = Trainer()
    model = APR(thres=0.5, epsilon=0.1, step=5, verbose=1)
    history = model.fit(X_train, y_train)
    y_pred = model.predict(X_test)
    
    y_pred_train = model.predict(X_train)
    crTrain = classification_report(y_train,y_pred_train, output_dict=True)
    logging.info('Train Accuracy: ' + str(crTrain['accuracy']))
    
    cr = classification_report(y_test,y_pred, output_dict=True)
    logging.info('Test Accuracy: ' + str(cr['accuracy']))
    cm = np.array2string(confusion_matrix(y_test,y_pred))
    
    results['ClassificationReport'] = str(cr)
    results['ConfustionMatrix'] = str(cm)
    results['TrainResult'] = crTrain['accuracy']
    results['TestResult'] = cr['accuracy']
    results['AUC'] = Evaluation.AUC_ROC(y_test,y_pred)
    results['y_test'] = y_test
    results['y_pred'] = y_pred
    
    
    return results, model


def PrepareData(df, colToRemove = None, model = None):
    df = df.drop(colToRemove, axis = 'columns')
    bags = list(df['BagName'].unique())
    
    X = list()
    y = list()

    col = list(df.columns)
    if 'BagName' in col:
        col.remove('BagName')
    if 'BagLabels' in col:
        col.remove('BagLabels')        
    if 'y' in col:
        col.remove('y')        
    if 'FileName' in col:
        col.remove('FileName')                
    for everyBag in bags:

        filterData = df.loc[df['BagName'] == everyBag].reset_index(drop = True)
        y.append(filterData.iloc[0]['BagLabels'])
        filterData = filterData[col]
        if model == None:
            X.append(np.array(filterData).reshape(1,-1))
        else:
            X.append(np.array(filterData))
        #instances_df.append(filterData)

    if model == None:
        X = np.concatenate(X, axis = 0)
    return X, y


def EvaluateRevised(BaggedData, clf, _evaal):
    results = {}
    colToRemove = list()
    for i in range(1,BaggedData.shape[1]):
        v = set(list(BaggedData.iloc[:, i]))
        if 0.0 in v and 0 in v and len(v) == 1:
            if((BaggedData.columns)[i] != 'BagLabels'):
                colToRemove.append((BaggedData.columns)[i])

    colToRemove.extend(['x1_mrc_0_1_to_0_5', 'x2_mrc_0_1_to_0_5', 'x3_mrc_0_1_to_0_5','x1_mrc_0_5_to_3', 'x2_mrc_0_5_to_3', 'x3_mrc_0_5_to_3','x1_mrc_3_and_more', 'x2_mrc_3_and_more', 'x3_mrc_3_and_more'])

    X, y = PrepareData(BaggedData, colToRemove, 'APR')
    y_pred=clf.predict(X)
    cr = classification_report(y, y_pred, output_dict = True)
    cm = np.array2string(confusion_matrix(y,y_pred))
    _evaal.AppendResult(cr['accuracy'])
    
    results['ClassificationReport'] = str(cr)
    results['ConfustionMatrix'] = str(cm)
    results['TestResult'] = cr['accuracy']
    results['AUC'] = Evaluation.AUC_ROC(y,y_pred)
    results['y_test'] = y
    results['y_pred'] = y_pred
    return results
    
def Prepare(pathToBes, pathToFes, p_id_train, p_id_test, window, overlap, windowOfAnnotations, destination, _train_test_split):
    logging.info('Check for Multiple pids...')
    pwd = p_id_train.split(';')
    logging.info('Total pwds: %s', len(pwd))

    _BagOfInstances = BoI.BoI()
    _ReadDirectory = Directory.HandlingDirectories()
    
    ListOfBaggedData = list()

    for p_id in pwd:
        logging.info('\nSubject : ' + p_id)
        dfs, FileNames = _ReadDirectory.ReadFiles(p_id, pathToFes, window, overlap)
        logging.info('Files Read: %s', str(len(dfs)))
        logging.info('Files FEs %s', ','.join(FileNames))

        Xs, FileNames = _ReadDirectory.ReadFiles(p_id, pathToBes, window, overlap)
        logging.info('Files Read: %s', str(len(Xs)))
        logging.info('Files BFs %s', ','.join(FileNames))
        
        logging.info('Classifer for ' + p_id)
        logging.info('Bagged Data \t' + p_id)
        BaggedData = _BagOfInstances.BagLabel(dfs, Xs, FileNames, windowOfAnnotations)
        _BagOfInstances.SetBagCounter(len(list(BaggedData['BagName'].unique())))
        logging.info('length \t' + str(len(BaggedData)))
        ListOfBaggedData.append(BaggedData)
    
    BaggedData = pd.concat(ListOfBaggedData)
    
    try:

        _evaal = Evaluation(p_id_train, destination + '061_Results/Training' , 'APR', window, overlap)
        results, clf = APR_MIL(BaggedData, _train_test_split)
        _evaal.SaveResult('Date: ' + time.strftime("%Y%m%d"))
        _evaal.SaveResult('Subject (Train): ' + p_id)
        _evaal.SaveResult('Window (in rows): ' +  str(window))
        _evaal.SaveResult('Overlap: ' + str(overlap))
        _evaal.SaveResult('Window of Annotations: ' + str(windowOfAnnotations))
        _evaal.SaveResult('Train-test split: ' + str(_train_test_split))
        for k in results:
            _evaal.SaveResult('\n\n' + str(k) + '\n' + str(results[k]))

        fName = ('CM' + '_APR_' + str(window) + '_' + str(overlap) + '_' + str(windowOfAnnotations))
        _evaal.SaveConfusionMatrix('Multiple pwd', 'APR', fName , results['y_test'], results['y_pred'], destination + '061_Results/Training/Images/')
        fName = ('AUC_ROC' + '_APR_' + str(window) + '_' + str(overlap) + '_' + str(windowOfAnnotations))
        _evaal.SaveAUCROC('Multiple pwd', 'APR', fName, results['y_test'], results['y_pred'], destination + '061_Results//Training/Images/')

        p_id_test = p_id_test.split(';')
        for p_test in p_id_test:
            _evaal = Evaluation(p_test, destination + '061_Results', 'APR', window, overlap)
            _evaal.SaveResult('Date: ' + time.strftime("%Y%m%d"))
            _evaal.SaveResult('Subject (Train): ' + p_id)
            _evaal.SaveResult('Subject (Test): ' + p_test)
            _evaal.SaveResult('Window (in rows): ' +  str(window))
            _evaal.SaveResult('Overlap: ' + str(overlap))
            _evaal.SaveResult('Window of Annotations: ' + str(windowOfAnnotations))
            _evaal.SaveResult('Train-test split: ' + str(_train_test_split))

            _evaal.AppendResult(p_id_train)
            _evaal.AppendResult(p_test)
            _evaal.AppendResult('APR')
            _evaal.AppendResult(results['TrainResult']) #Training Result


            logging.info('Testing for ' + p_test)

            dfs, FileNames = _ReadDirectory.ReadFiles(p_test, pathToFes, window, overlap)
            logging.info('Files Read: %s', str(len(dfs)))
            logging.info('Files FEs %s', ','.join(FileNames))

            Xs, FileNames = _ReadDirectory.ReadFiles(p_test, pathToBes, window, overlap)
            logging.info('Files Read: %s', str(len(Xs)))
            logging.info('Files BFs %s', ','.join(FileNames))

            logging.info('Bagged Data for testing: \t' + p_test)
            BaggedData = _BagOfInstances.BagLabel(dfs, Xs, FileNames, windowOfAnnotations)
            logging.info(p_test)
            results_test = EvaluateRevised(BaggedData, clf, _evaal)
            for k in results_test:
                _evaal.SaveResult('\n\n' + str(k) + '\n' + str(results_test[k]))

            path = destination + '061_Results/' + p_test + '/Images/'
            fName = ('CM' + '_APR_' + str(p_test) + '_' + str(window) + '_' + str(overlap) + '_' + str(windowOfAnnotations))
            _evaal.SaveConfusionMatrix(p_test, 'APR', fName , results_test['y_test'], results_test['y_pred'], path)
            fName = ('AUC_ROC' + '_APR_' + str(p_test) + '_' + str(window) + '_' + str(overlap) + '_' + str(windowOfAnnotations))
            _evaal.SaveAUCROC(p_test, 'APR', fName, results_test['y_test'], results_test['y_pred'], path)

            _evaal.AppendResult(_train_test_split)
            _evaal.AppendResult(window)
            _evaal.AppendResult(windowOfAnnotations)
            _evaal.CombinedResultAsDf()
            
    except Exception as e:
        print(e)
        _evaal.ResetResultForDf(p_id, 'APR')
        _evaal.CombinedResultAsDf()
            
            
        

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument("source_fes", help="Path to read files.",type=str)
    parser.add_argument("source_bfs", help="Path to read files.",type=str)
    parser.add_argument("destination", help="Path to save files.",type=str)
    parser.add_argument("pwd_train", help="Person with dementia id for training",type=str)
    parser.add_argument("pwd_test", help="Person with dementia id for test",type=str)
    parser.add_argument("window", help="Feature extraction window, in seconds.",type=int, default=60)
    parser.add_argument("overlap", help="Overlap of windows, in percentage (no percentage sign).",type=int,default=50)
    parser.add_argument("window_annotations", help="Annotations window, in minutes.",type=int,default=20)
    parser.add_argument("_train_test_split", help="train_test_split, in percentage (no percentage sign).",type=int,default=30)
    parser.add_argument("isBinaryClassification", help="isBinaryClassification, 1 = True, 0 = False.",type=int,default=1)
    
    args = parser.parse_args()
    
    source_fes = args.source_fes 
    source_bfs = args.source_bfs 
    destination = args.destination
    p_id_train = args.pwd_train
    p_id_test = args.pwd_test
    window = args.window
    overlap = args.overlap
    aWindow = args.window_annotations
    _train_test_split = args._train_test_split
    _isBinaryClassification = args.isBinaryClassification
    
    ReadDirectory = Directory.HandlingDirectories()
    ReadDirectory.CreatePathIfNotExists(destination + '/091_Logging')
    
    
    logging.basicConfig(filename= destination + '/091_Logging' + '/APR_'+str(window)+'_'+str(overlap)+'_'+str(aWindow)+'.log', level=logging.INFO, filemode="w")
    logging.info('Argument read...:')
    logging.info('Source FEs: %s', str(source_fes))
    logging.info('Source BEs: %s', str(source_bfs))
    logging.info('Destination: %s', str(destination))
    logging.info('Training pids: %s', str(p_id_train))
    logging.info('Test pids: %s', str(p_id_test))
    logging.info('Window (in rows): %s', str(window))
    logging.info('Overlap: %s', str(overlap) + '%')
    logging.info('Window Annotations: %s', str(aWindow) + '%')
    logging.info('Train-test split: %s', str(_train_test_split) + '%')
    logging.info('isBinaryClassification: %s', str(_isBinaryClassification))
    
    if _isBinaryClassification == 0:
        logging.info('****ERROR*****Axis Parallel can only be binary****ERROR*****')
        print('****ERROR*****Axis Parallel can only be binary****ERROR*****')
        exit(-1)

    Prepare(source_bfs, source_fes, p_id_train, p_id_test, int(window)*50, overlap, aWindow, destination, float(_train_test_split/100))