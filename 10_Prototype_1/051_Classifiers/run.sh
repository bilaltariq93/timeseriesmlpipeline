# 1. source
# 2. destination
# 3. pwd
# 4. window (In seconds) (Optional)
# 5. overlap (In pecentage) (Optional)
# 6. window of annotations (in minutes) (Optional)
# 7. _train_test_split (in percentage) (Optional, for analysis purpose)
# 8. _isBinaryClassification (1 means binary classification, 0 means multiclass classification, APR won't work on multiclass classification)

set _sourceFEs = "/mnt/mmis-insidedem/DataBT/ThesisPrototype_8/040_FeatureExtracted/"
set _sourceBFs = "/mnt/mmis-insidedem/DataBT/ThesisPrototype_8/020_BandPassFilter/"
set _destination = "/mnt/mmis-insidedem/DataBT/ThesisPrototype_7_5/"
set _pwd_train = "X110;X111;X113;X114;X121;X122;X126"
set _pwd_test = "X124"# "X110;X111;X113;X114;X121;X122;X124;X126"
set _isBinaryClassification = 1
set _overlap = 50
set _window = 60
set _windowOfAnnotations = 5
set _train_test_split = 30

#python3 Axis_Parallel_Recectangle_MIL.py $_sourceFEs $_sourceBFs $_destination $_pwd_train $_pwd_test $_window $_overlap $_windowOfAnnotations $_train_test_split $_isBinaryClassification
#python3 Boosting_Adaboost_MIL.py $_sourceFEs $_sourceBFs $_destination $_pwd_train $_pwd_test $_window $_overlap $_windowOfAnnotations $_train_test_split $_isBinaryClassification 
#python3 SVM_MIL.py $_sourceFEs $_sourceBFs $_destination $_pwd_train $_pwd_test $_window $_overlap $_windowOfAnnotations $_train_test_split $_isBinaryClassification
python3 SVM_SIL.py $_sourceFEs $_sourceBFs $_destination $_pwd_train $_pwd_test $_window $_overlap $_windowOfAnnotations $_train_test_split $_isBinaryClassification
# python3 RandomForest_SIL.py $_sourceFEs $_sourceBFs $_destination $_pwd_train $_pwd_test $_window $_overlap $_windowOfAnnotations $_train_test_split $_isBinaryClassification
# python3 Boosting_Adaboost_SIL.py $_sourceFEs $_sourceBFs $_destination $_pwd_train $_pwd_test $_window $_overlap $_windowOfAnnotations $_train_test_split $_isBinaryClassification
